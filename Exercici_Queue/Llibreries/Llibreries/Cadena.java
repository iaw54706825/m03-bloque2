package Llibreries;

public class Cadena {
	
	public static boolean stringIsInt(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	/**
	 * Comprueba que cada elemento de un array de strings 
	 * @param array
	 * @param numElementos
	 * @return
	 */
	public static boolean allParametresInt(String[] array, int numElementos) {
		int i = 0;
		while (i < array.length && Cadena.stringIsInt(array[i]) ) {
			i++;
		} 
		return i == numElementos;
	}
	
	public static int[] arrayStringToInt(String[] array) {
		int[] nuevoArray = new int[array.length];
		for(int i = 0; i < array.length; i++) {
			nuevoArray[i] = Integer.parseInt(array[i]); 
		}
		return nuevoArray;
	}
}
