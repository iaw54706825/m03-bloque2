package Varies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Data {
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	/**
	 * Devuelve un LocalDateTime en formato : "dd-MM-yyyy HH:mm"
	 * 
	 * @param date
	 * @return
	 */
	public static LocalDateTime format(String date) {
		return LocalDateTime.parse(date, formatter);
	}

	/**
	 * imprimir Data
	 */
	public static String imprimirData(LocalDateTime dataTmp) {
		if (dataTmp == null) return "NULL";
		return dataTmp.format(formatter);
	}
}
