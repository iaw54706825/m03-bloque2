package com.daw.carlosbernuy.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Ruta_Dades implements Comparable<Ruta_Dades>{
	private int id;
	private String nom;
	private ArrayList<Integer> waypoints;
	private boolean actiu;
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;
	private LocalDateTime dataModificacio;

	/**
	 * @param id
	 * @param nom
	 * @param waypoints
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	/**
	 * @return the waypoints
	 */
	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	
	public String imprimirDades() {
		return  nom + ": waypoints" + waypoints;
	}
	

	
	@Override
	public int compareTo(Ruta_Dades o) {
		if (this.waypoints.equals(o.waypoints)) {
			return 0;
		}
		return o.id - this.id;
	}
	
	@Override
	public String toString() {
		return " id = " + id + ":\n nom = " + nom + "\n waypoints = " + waypoints + "\n actiu = " + actiu
				+ "\n dataCreacio = " + dataCreacio + "\n dataAnulacio = " + dataAnulacio + "\n dataModificacio = "
				+ dataModificacio;
	}


}
