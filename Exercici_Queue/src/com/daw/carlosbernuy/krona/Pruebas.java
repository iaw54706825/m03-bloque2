package com.daw.carlosbernuy.krona;

import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import Llibreries.Cadena;
import Varies.Data;

public class Pruebas {

	public static void main(String[] args) {
	    HashMap<String, Double> hm = new HashMap<String, Double>();

        // Put elements to the HashMap.
        hm.put("Zara", new Double(3434.34));
        hm.put("Mahnaz", new Double(123.22));
        hm.put("Ayan", new Double(1378.00));
        hm.put("Daisy", new Double(99.22));
        hm.put("Qadir", new Double(-19.08));
        System.out.println(hm.get("zara"));
	}
}
