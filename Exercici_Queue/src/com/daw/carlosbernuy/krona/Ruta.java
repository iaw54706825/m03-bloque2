package com.daw.carlosbernuy.krona;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import Llibreries.Cadena;

import java.util.Scanner;
import Varies.Data;

public class Ruta {

	/**
	 * 
	 * @return
	 */
	public static List<Waypoint_Dades> crearRutaInicial() {
		List<Waypoint_Dades> llistaWaypointLinkedList = null;

		llistaWaypointLinkedList = new LinkedList<Waypoint_Dades>();

		llistaWaypointLinkedList.add(new Waypoint_Dades(0, "�rbita de la Terra", new int[] { 0, 0, 0 }, true,
				LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] { 1, 1, 1 }, true,
				LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(2, "�rbita de la LLuna", new int[] { 2, 2, 2 }, true,
				LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(3, "�rbita de Mart", new int[] { 3, 3, 3 }, true,
				LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(4, "�rbita de J�piter", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(5, "Punt Lagrange J�piter-Europa", new int[] { 5, 5, 5 }, true,
				LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(6, "�rbita de Europa", new int[] { 6, 6, 6 }, true,
				LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(7, "�rbita de Venus", new int[] { 7, 7, 7 }, true,
				LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));

		return llistaWaypointLinkedList;
	}

	/**
	 * Ejercicio 4
	 * 
	 * @param comprovacioRendimentTmp
	 * @return
	 */
	public static ComprovacioRendiment inicialitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		List<Waypoint_Dades> waypoints = crearRutaInicial();
		for (Waypoint_Dades waypoint : waypoints) {
			comprovacioRendimentTmp.pilaWaypoints.push(waypoint);
		}
		Waypoint_Dades waypoint = new Waypoint_Dades(4, "�rbita de J�piter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		comprovacioRendimentTmp.pilaWaypoints.push(waypoint);
		comprovacioRendimentTmp.wtmp = waypoint;
		return comprovacioRendimentTmp;
	}

	/**
	 * Ejercicio 5
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Iterator<Waypoint_Dades> it = comprovacioRendimentTmp.pilaWaypoints.iterator();
		System.out.println("La ruta est� formada pels waypoints: ");
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	/**
	 * Ejercicio 6
	 *
	 * @param comprovacioRendimentTmp
	 */
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
		for (Waypoint_Dades waypoint_Dades : comprovacioRendimentTmp.pilaWaypoints) {
			pilaWaypointsInversa.push(waypoint_Dades);
		}
		for (Waypoint_Dades waypoint_Dades : pilaWaypointsInversa) {
			System.out.println(waypoint_Dades);
		}
	}

	/**
	 * Ejercicio 7
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println(
				"Si hem trobat el waypoint Orbita de Jupiter REPETIDA emmagatzemat en comprovacioRendimentTMP.wtmp, en la llista");
		System.out
				.print("Resultado de 'comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp)': ");
		System.out.println(comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp));
		Waypoint_Dades waypoint = new Waypoint_Dades(4, "�rbita de J�piter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		System.out.println("\nNO hem trobat el waypoint Orbita de Jupiter creat ara mateix, en la llista");
		System.out.print("Resultado de 'comprovacioRendimentTmp.pilaWaypoints.contains(waypointMismosdatos)': ");
		System.out.println(comprovacioRendimentTmp.pilaWaypoints.contains(waypoint));
	}

	// ------------Ejercicios Set---------------
	/**
	 * Ejercicio 4
	 * 
	 * @param comprovacioRendimentTmp
	 * @return
	 */
	public static ComprovacioRendiment inicialitzaLListaRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta 0: Terra --> Punt Lagrange J�piter-Europa",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true,
				LocalDateTime.parse("28-10-2020 16:30", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:30", Data.formatter));
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> �rbita de Mart (directe)",
				new ArrayList<Integer>(Arrays.asList(0, 3)), true,
				LocalDateTime.parse("28-10-2020 16:31", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:31", Data.formatter));
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> �rbita de Venus",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter));
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> �rbita de J�piter ",
				new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true,
				LocalDateTime.parse("28-10-2020 16:33", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:33", Data.formatter));
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta 2.2: Terra --> �rbita de Venus (REPETIDA)",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null,
				LocalDateTime.parse("30-10-2020 19:49", Data.formatter));
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);

		// Imprimimos lista de rutas
		System.out.println("Insertades les rutes:");
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			System.out.println(ruta.imprimirDades());
		}
		return comprovacioRendimentTmp;
	}

	/**
	 * Ejercicio 5
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		HashSet<Integer> waypoints = new HashSet<Integer>();

		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			waypoints.addAll(ruta.getWaypoints());
		}
		System.out.println("ID dels waypoints ficats en el set: " + waypoints);
	}

	/**
	 * Ejercicio 6
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Integer> waypoints = new HashSet<Integer>();
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			waypoints.addAll(ruta.getWaypoints());
		}

		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			waypoints.retainAll(ruta.getWaypoints());
		}
		System.out.println("ID dels waypoints en totes les rutes:" + waypoints);
	}

	/**
	 * Ejercicio 7
	 * 
	 * @param numRuta
	 * @param comprovacioRendimentTmp
	 * @return posicio ruta
	 */
	private static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		int posicio = -1;
		int i = 0;
		while (i < comprovacioRendimentTmp.llistaRutes.size()) {
			if (numRuta == comprovacioRendimentTmp.llistaRutes.get(i).getId()) {
				posicio = i;
				break;
			}
			i++;
		}
		return posicio;
	}

	/**
	 * Ejercicio 8
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String datos = "";
		Set<Integer> waypoints = new HashSet<Integer>();
		System.out.println("Rutes actuals:");
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			System.out.print("ID " + ruta.getId() + ": ");
			System.out.println(ruta.imprimirDades());
		}

		// Comprobamos que sean dos parametros, que sean todos digitos, y que cada
		// digito exista como ID
		while (datos.split(" ").length != 2 || !Cadena.allParametresInt(datos.split(" "), 2)
				|| Ruta.buscarRuta(Integer.parseInt(datos.split(" ")[0]), comprovacioRendimentTmp) == -1
				|| Ruta.buscarRuta(Integer.parseInt(datos.split(" ")[1]), comprovacioRendimentTmp) == -1) {
			System.out.println("Selecciona la ruta A i B (format: 3 17)");
			datos = s.nextLine();
			if (datos.split(" ").length != 2) {
				System.out.println("ERROR: Introduir 2 parametres separats per 1 espai en blanc. Has introduit "
						+ datos.split(" ").length + " parametres");
			} else if (!Cadena.allParametresInt(datos.split(" "), 2)) {
				System.out.println("ERROR: Els id de les rutes son integers");
			} else if (Ruta.buscarRuta(Integer.parseInt(datos.split(" ")[0]), comprovacioRendimentTmp) == -1) {
				System.out.println("ERROR: no existeix la ruta " + datos.split(" ")[0]);
			} else if (Ruta.buscarRuta(Integer.parseInt(datos.split(" ")[1]), comprovacioRendimentTmp) == -1) {
				System.out.println("ERROR: no existeix la ruta " + datos.split(" ")[1]);
			} else {
				// A�adimos los la lista de waypoints a partir de la ruta indicada.
				// Integer.parseInt(datos.split(" ")[0]) <-------- Nos indica la ruta A
				waypoints.addAll(
						comprovacioRendimentTmp.llistaRutes.get(Integer.parseInt(datos.split(" ")[0])).getWaypoints());
				System.out.println("HashSet (havent-hi afegir els waypoints de la ruta A) = " + waypoints);

				// A�adimos los la lista de waypoints a partir de la ruta indicada.
				// Integer.parseInt(datos.split(" ")[1]) <-------- Nos indica la ruta B
				waypoints.removeAll(
						comprovacioRendimentTmp.llistaRutes.get(Integer.parseInt(datos.split(" ")[1])).getWaypoints());
				System.out.println("HashSet (havent-hi tret els waypoints de la ruta B) = " + waypoints);
			}
		}
	}

	/**
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		SortedSet<Ruta_Dades> listaOrdenada = new TreeSet<Ruta_Dades>();
		listaOrdenada.addAll(comprovacioRendimentTmp.llistaRutes);
		// System.out.println(listaOrdenada);
		for (Ruta_Dades ruta : listaOrdenada) {
			System.out.println("ID " + ruta.getId() + ": " + ruta.imprimirDades());
		}

	}

	// -----------------------------------------
	// ----------Ejercicios MAP-----------------
	// -----------------------------------------

	/**
	 * Ejercicio 4
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void crearLinkedHashMapDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		long tempsIni, tempsFinal, temps1, temps2, temps3;
		Map<Integer, Ruta_Dades> mapRutes = new LinkedHashMap<Integer, Ruta_Dades>();
		for (Ruta_Dades ruta : comprovacioRendimentTmp.llistaRutes) {
			mapRutes.put(ruta.getId(), ruta);
		}

		// 1ra forma
		System.out.println("1a forma de visualitzar el contingut del map (map --> set + iterador del set):");
		Set set = mapRutes.entrySet();
		Iterator it = set.iterator();
		tempsIni = System.nanoTime();
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry) it.next();
			System.out.println("Clau del map = " + me.getKey() + ": \nDades de la ruta:\n" + me.getValue());
		}
		tempsFinal = System.nanoTime();
		temps1 = tempsFinal - tempsIni;

		// 2da forma
		System.out.println("\n2a forma de visualitzar el contingut del map (iterator de les claus del map):");
		int clau;
		Iterator<Integer> it2 = mapRutes.keySet().iterator();
		tempsIni = System.nanoTime();
		while (it2.hasNext()) {
			clau = it2.next();
			System.out.println("Clau del map = " + clau + ":\nDades de la ruta:\n" + mapRutes.get(clau));
		}
		tempsFinal = System.nanoTime();
		temps2 = tempsFinal - tempsIni;

		// 3a forma
		System.out.println("\n3a forma de visualitzar el contingut del map (for-each del map --> set):");
		tempsIni = System.nanoTime();
		for (Entry<Integer, Ruta_Dades> dada : mapRutes.entrySet()) {
			System.out.println("Clau del map = " + dada.getKey() + ":\nDades de la ruta:\n" + dada.getValue());
			// System.out.println("Clau del map = " + dada.getKey() + ":\nDades de la
			// ruta:\n"+ dada.getValue().toString());
		}
		tempsFinal = System.nanoTime();
		temps3 = tempsFinal - tempsIni;
		// Aqui lo divido entre 1000, por que sino el resultado de convertirlo a
		// milisegundos es Cero(0), por que hay pocas impresiones
		System.out.println("\nTEMPS PER 1a FORMA (map -- >set + iterador del set):" + (temps1 / 1000));
		System.out.println("TEMPS PER 2a FORMA (iterator de les claus del map):" + (temps2 / 1000));
		System.out.println("TEMPS PER 3a FORMA (for-each del map --> set):" + (temps3 / 1000));

		// Introducimos rutas Map
		for (Entry<Integer, Ruta_Dades> dada : mapRutes.entrySet()) {
			comprovacioRendimentTmp.mapaLinkedDeRutes.put(dada.getKey(), dada.getValue());
		}
	}

	/**
	 * Ejercicio 5
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void visualitzarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String opcio = "";
		// Condicionamos que sea un parametros y que sea un entero
		while (opcio.split(" ").length != 1 || !Cadena.stringIsInt(opcio)) {
			System.out.print("Escriu el n� del waypoint que vols buscar: ");
			opcio = s.nextLine();
			if (opcio.split(" ").length != 1) {
				System.out.println("ERROR: Introduci un valor");
			} else if (!Cadena.stringIsInt(opcio)) {
				System.out.println("ERROR: has introduit " + opcio + " com a ruta. Els ID del res rutes son integers");
			} else {
				System.out.println("RUTES QUE CONTENES EL WAYPOINT " + opcio + ":\n");
				int opcioToInt = Integer.parseInt(opcio);
				// Comprobamos si contiene el waypoint dado
				for (Entry<Integer, Ruta_Dades> dada : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
					if (dada.getValue().getWaypoints().contains(opcioToInt)) {
						System.out.println(dada.getValue());
					}
				}
			}
		}
	}

	/**
	 * Ejercicio 6
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void esborrarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String opcio = "";
		// Condicionamos que sea un parametros y que sea un entero
		while (opcio.split(" ").length != 1 || !Cadena.stringIsInt(opcio)) {
			System.out.print("Escriu el n� del waypoint que vols buscar: ");
			opcio = s.nextLine();
			if (opcio.split(" ").length != 1) {
				System.out.println("ERROR: Introduci un valor");
			} else if (!Cadena.stringIsInt(opcio)) {
				System.out.println("ERROR: has introduit " + opcio + " com a ruta. Els ID del res rutes son integers");
			} else {
				System.out.println("RUTES ESBORRADES QUE CONTENES EL WAYPOINT " + opcio + ":");
				int opcioToInt = Integer.parseInt(opcio);
				// Borramos con Iterator
				Iterator<Integer> it = comprovacioRendimentTmp.mapaLinkedDeRutes.keySet().iterator();
				while (it.hasNext()) {
					Integer key = it.next();
					// Si un array de un LinkedMap contiene una ruta, la eliminamos
					if (comprovacioRendimentTmp.mapaLinkedDeRutes.get(key).getWaypoints().contains(opcioToInt)) {
						System.out.println("dades de la ruta:");
						System.out.println(comprovacioRendimentTmp.mapaLinkedDeRutes.get(key));
						it.remove();
					}
				}
			}
		}
	}

	/**
	 * Ejercicio 7
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void visualitzarUnaRutaDelMap(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String opcio = "";
		while (opcio.split(" ").length != 1 || !Cadena.stringIsInt(opcio)) {
			System.out.print("Escriu el ID de la ruta que vols buscar:");
			opcio = s.nextLine();
			if (opcio.split(" ").length != 1) {
				System.out.println("ERROR: Introduci un valor");
			} else if (!Cadena.stringIsInt(opcio)) {
				System.out.println("ERROR: has introduit " + opcio + " com a ruta. Els ID del res rutes son integers");
			} else {
				int opcioToInt = Integer.parseInt(opcio);
				if (comprovacioRendimentTmp.mapaLinkedDeRutes.containsKey(opcioToInt)) {
					System.out.println(comprovacioRendimentTmp.mapaLinkedDeRutes.get(opcioToInt));
				} else {
					System.out.println("");
				}
			}
		}
	}
	
	/**
	 * Ejercicio 8
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void ordenarRutesMapPerID(ComprovacioRendiment comprovacioRendimentTmp) {
		SortedMap<Integer, Ruta_Dades> rutasOrdenadas = new TreeMap<Integer, Ruta_Dades>();
		//Lo copiamos en un TreeMap, se ordenara por la llave dada, en este caso enteros
		//		mapaTreeRutes.putAll(comprovacioRendimentTmp.mapaLinkedDeRutes);
		for (Entry<Integer, Ruta_Dades> dada : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
			rutasOrdenadas.put(dada.getKey(), dada.getValue());
		}
		for (Entry<Integer, Ruta_Dades> dada : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
			System.out.println(dada.getKey() + ": Dades de la ruta:");
			System.out.println(dada.getValue());
		}
	}
	
//	//35. Ordenar les rutes del MAP per waypoints i ID (el ID de m�s gran a m�s petit)
//	// Per defecte ordena per la clau. Ara volem que ordeni pel valor, per aix� far� servir
//	// el compareTo() del valor (el de la classe Ruta_Dades).
//	//
//	// Les Ruta_Dades que tinguin els mateixos waypoints fa que siguin iguals (est� definit en el compareTo() de Ruta_Dades) i
//	// per tant nom�s hi pot haver 1 en el TreeSet (la 1a que fiquem).
//	public static void ordenarRutesMapPerWaypointsAndID(ComprovacioRendiment comprovacioRendimentTmp) {
//		SortedMap<Integer, Ruta_Dades> mapaTreeRutes;
//		
//		mapaTreeRutes = new TreeMap(new comparadorPerWaypointAndID(comprovacioRendimentTmp.mapaLinkedDeRutes));
//		
//		//mapaTreeRutes = new TreeMap();
//
//		
//		mapaTreeRutes.putAll(comprovacioRendimentTmp.mapaLinkedDeRutes);
//		
//		for (Entry<Integer, Ruta_Dades> dada : mapaTreeRutes.entrySet()) {
//            System.out.println(dada.getKey() + ": " + dada.getValue().toString());
//        }

}
