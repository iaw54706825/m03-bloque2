package com.daw.carlosbernuy.krona;


import java.util.Comparator;
import java.util.LinkedHashMap;

public class comparadorPerWaypointAndID implements Comparator {
	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	
	
	

	public comparadorPerWaypointAndID(LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes) {
		this.mapaLinkedDeRutes = mapaLinkedDeRutes;
	}



	// Al fer un MapTree fent servir aquest Comparator, es far� servir la funci� compare()
    // per a determinar la posici� en l'arbre ordenat i evidentment no hi podran haver elements 
    // repetits dins de l'arbre (la funci� compare() determina quan 2 elements s�n iguals
    // perqu� ocuparan la mateixa posici� d'ordenaci� en l'arbre). 
	//
    // Com est� agafant objectes de tipus Ruta_Dades, quan �s fa "return valorA.compareTo(valorB)"
    // en realitat est� fent servir la funci� compareTo() que hi ha dins de Ruta_Dades la qual est� 
    // comparant dues rutes en funci� dels waypoints i del ID.
	@Override
	public int compare(Object o1, Object o2) {
		Comparable valorA = (Comparable) mapaLinkedDeRutes.get(o1);
        Comparable valorB = (Comparable) mapaLinkedDeRutes.get(o2);
        
        return valorA.compareTo(valorB);
	}

}