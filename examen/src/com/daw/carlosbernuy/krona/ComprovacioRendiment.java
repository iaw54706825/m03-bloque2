package com.daw.carlosbernuy.krona;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ComprovacioRendiment {
	int[] coordenadesTmp = null;
	List<Waypoint_Dades> llistaArrayList;
	List<Waypoint_Dades> llistaLinkedList;
	Waypoint_Dades wtmp;
	public Deque<Waypoint_Dades> pilaWaypoints;
	public ArrayList<Ruta_Dades> llistaRutes;
	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	LinkedList<Waypoint_Dades> llistaWaypoints = new LinkedList<Waypoint_Dades>();
	LinkedHashMap<Integer, Waypoint_Dades> mapaWaypoints;

	public ComprovacioRendiment() {
		// DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

		this.coordenadesTmp = new int[] { 0, 0, 0 };
		this.llistaArrayList = new ArrayList<Waypoint_Dades>(); //
		this.llistaLinkedList = new LinkedList<Waypoint_Dades>();
		this.pilaWaypoints = new ArrayDeque<Waypoint_Dades>();
		this.wtmp = null;
		this.llistaRutes = new ArrayList<Ruta_Dades>();
		this.mapaLinkedDeRutes = new LinkedHashMap<Integer, Ruta_Dades>();
		this.mapaWaypoints = new LinkedHashMap<Integer, Waypoint_Dades>();
		// this.waypoint = new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp,
		// true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
	}
}
