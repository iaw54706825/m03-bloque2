/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw.carlosbernuy.krona;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author gmartinez
 */
public class Krona {
    /*
    SISTEMA DE NAVEGACIÓ BASAT EN WAYPOINTS.
    ES DONEN D'ALTA DIVERSOS WAYPOINTS DE L'ESPAI (ORBITA MARCIANA, PUNT LAGRANGE TERRA-LLUNA, PHOBOS, SATURN, LLUNA,...).
    ES PODEN MEMORITZAR DIVERSES RUTES AFEGINT DIVERSOS WAYPOINTS A CADA RUTA.
    
    */
    
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("\nToca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }
    
    
    public static void menuKrona() throws IOException  {
        String opcio;
        Scanner sc = new Scanner(System.in);
        StringBuilder menu = new StringBuilder("");
        
        ComprovacioRendiment comprovacioRendimentTmp = null;
        
        do {
            menu.delete(0, menu.length());
            
            menu.append(System.getProperty("line.separator"));
            menu.append("RV-18A Krona ");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator")); 
            
            menu.append("1. Inicialitzar ComprovacioRendiment");
            menu.append(System.getProperty("line.separator"));
            menu.append("2. Comprovar rendiment d'inicialitzacio d'un ArrayList i un LinkedList de waypoints");
            menu.append(System.getProperty("line.separator"));
            menu.append("3. Comprovar rendiment d'insercio en un ArrayList i en un LinkedList de waypoints");
            menu.append(System.getProperty("line.separator"));
            menu.append("4. Modificacio dels waypoints de l'ArrayList i del LinkedList");
            menu.append(System.getProperty("line.separator"));
            menu.append("5. Esborrar waypoints de l'ArrayList i del LinkedList");
            menu.append(System.getProperty("line.separator"));
            menu.append("6. Modificar coordenades i nom dels waypoint de l'ArrayList amb ID parell");
            menu.append(System.getProperty("line.separator"));
            menu.append("7. Visualitzar els waypoint de l'ArrayList ordenats");
            menu.append(System.getProperty("line.separator"));
            menu.append("8. Llistar els waypoint de l'ArrayList que es trobin a certa distancia de la Terra");
            
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            menu.append("10. Inicialitzar una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("11. Visualitzar una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("12. Invertir una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("13. Existeis el waypoint 'Orbita de Jupiter REPETIDA' en la ruta?");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));

            menu.append("20. Inicialitzar llista de rutes");
            menu.append(System.getProperty("line.separator"));
            menu.append("21. Ficar tots els waypoint en un set");
            menu.append(System.getProperty("line.separator"));
            menu.append("22. Waypoints que estan en totes les rutes");
            menu.append(System.getProperty("line.separator"));
            menu.append("23. Waypoints que estan en la ruta A i no en la ruta B");
            menu.append(System.getProperty("line.separator"));
            menu.append("24. Ordenas les rutes per waypoints i ID (el ID de mes gran a mes petit)");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));

            menu.append("30. Crear un MAP de rutes i visualitzar-li");
            menu.append(System.getProperty("line.separator"));
            menu.append("31. Visualitza totes les rutes del MAP que tinguin un waypoint concret");
            menu.append(System.getProperty("line.separator"));
            menu.append("32. Esborrar totes les rutes del MAP que tinguin un waypoint concret");
            menu.append(System.getProperty("line.separator"));
            menu.append("33. Visualitzar les dades d'una ruta");
            menu.append(System.getProperty("line.separator"));
            menu.append("34. Ordenar les rutes del MAP pel ID");
            menu.append(System.getProperty("line.separator"));
            menu.append("35. Ordenar les rutes del MAP per waypoints i ID (el ID de mes gran a mes petit)");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));
            
            menu.append("40. Inicialitzar waypoints");
            menu.append(System.getProperty("line.separator"));
            menu.append("41. Insertar un nou waypoint");
            menu.append(System.getProperty("line.separator"));
            menu.append("42. Visualitzar els waypoints versus un tipus");
            menu.append(System.getProperty("line.separator"));
            menu.append("43. Visualitzar el nº de waypoints versus el seu tipus");
            menu.append(System.getProperty("line.separator"));
            menu.append("44. Trobar waypoints versus el seu nom");
            menu.append(System.getProperty("line.separator"));
            menu.append("45. Ordenar els waypoints per data");
            menu.append(System.getProperty("line.separator"));
            menu.append(System.getProperty("line.separator"));

            
            menu.append("50. Tornar al menu pare (PNS-24 Puma)");
            menu.append(System.getProperty("line.separator"));
            
            
            System.out.print(MenuConstructorPantalla.constructorPantalla(menu));
            
            opcio = sc.next();
            
            
            switch (opcio) {
                case "1":
                	comprovacioRendimentTmp = Waypoint.inicialitzarComprovacioRendiment();
                    bloquejarPantalla();
                    break;
                case "2":
                	comprovacioRendimentTmp = Waypoint.comprovarRendimentInicialitzacio(10, comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "3":
                	comprovacioRendimentTmp = Waypoint.comprovarRendimentInsercio(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "4":
                	comprovacioRendimentTmp = Waypoint.modificarWaypoints(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "5":
                	comprovacioRendimentTmp = Waypoint.esborrarWaypoints(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "6":
                	comprovacioRendimentTmp = Waypoint.modificarCoordenadesINomDeWaypoints(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "7":
                	Waypoint.visualitzarWaypointsOrdenats(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "8":
                	Waypoint.waypointsACertaDistanciaMaxDeLaTerra(comprovacioRendimentTmp);
                    bloquejarPantalla();
                	break;
                case "10":
                	comprovacioRendimentTmp = Ruta.inicialitzarRuta(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;    
                case "11":
                    Ruta.visualitzarRuta(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "12":   
                	Ruta.invertirRuta(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "13":
                    Ruta.existeixWaypointEnRuta(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "20":
                	comprovacioRendimentTmp = Ruta.inicialitzaLListaRutes(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "21":
                    Ruta.setUnio(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break;
                case "22":
                    Ruta.setInterseccio(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "23":
                    Ruta.setResta(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "24":
                    Ruta.crearSetOrdenatDeRutes(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "30":
                    Ruta.crearLinkedHashMapDeRutes(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "31":
                    Ruta.visualitzarRutesDelMapAmbUnWaypointConcret(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "32":
                    Ruta.esborrarRutesDelMapAmbUnWaypointConcret(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "33":
                    Ruta.visualitzarUnaRutaDelMap(comprovacioRendimentTmp);
                    bloquejarPantalla();
                case "34":
                    Ruta.ordenarRutesMapPerID(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "40":
                    Waypoint.InicialitzarDadesWaypoint(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "41":
                    Waypoint.nouWaypoint(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "42":
                    Waypoint.waypointsVsTipus(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "43":
                    Waypoint.numWaypointsVsTipus(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "44":
                    Waypoint.trobarWaypointVsNom(comprovacioRendimentTmp);
                    bloquejarPantalla();
                    break; 
                case "50":
                    break; 
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }   
        } while (!opcio.equals("50"));
    }
    
}
