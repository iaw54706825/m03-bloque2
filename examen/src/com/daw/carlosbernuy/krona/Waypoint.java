package com.daw.carlosbernuy.krona;

import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import Llibreries.Cadena;
import Varies.Data;

public class Waypoint {

	/*
	 * METODO 1
	 */
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		return comprovacioRendimentTmp;
	}

	/*
	 * METODO 2
	 */
	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimentTmp) {
		long tiempoInicial;
		long tiempoFinal;
		long tiempoTotal;

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int coordenades[] = { 0, 0, 0 };// Cordenada para insertar en cada objeto

		// Insertamos ArrayList
		tiempoInicial = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			// llistaArrayList
			comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0, "Orbita de la terra", coordenades, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		}
		tiempoFinal = System.nanoTime();
		tiempoTotal = tiempoFinal - tiempoInicial;
		System.out.println("Tiempo para insertar " + numObjACrear + " waypoints en l'ArrayList: "
				+ TimeUnit.MILLISECONDS.convert(tiempoTotal, TimeUnit.NANOSECONDS));

		// Insertamos LinkedList
		tiempoInicial = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0, "Orbita de la terra", coordenades, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		}
		tiempoFinal = System.nanoTime();
		tiempoTotal = tiempoFinal - tiempoInicial;
		System.out.println("Tiempo para insertar " + numObjACrear + " waypoints en LinkedList: "
				+ TimeUnit.MILLISECONDS.convert(tiempoTotal, TimeUnit.NANOSECONDS));

		return comprovacioRendimentTmp;
	}

	/*
	 * METODO 3
	 */
	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		long tiempoInicial;
		long tiempoFinal;
		int mitad;

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int coordenades[] = { 0, 0, 0 };// Cordenada para insertar en cada objeto

		mitad = comprovacioRendimentTmp.llistaArrayList.size() / 2;
		System.out.println(
				"llistaArraList.size():" + comprovacioRendimentTmp.llistaArrayList.size() + ", meitatLlista:" + mitad);

		// INSERTAMOS PRIMERA POSICION ArrayList
		tiempoInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(0, new Waypoint_Dades(0, "Orbita de la terra", coordenades, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		tiempoFinal = System.nanoTime();
		System.out.println(
				"Temps per insertar 1 waypoint en la 1ra posicio de l'ArrayList: " + (tiempoFinal - tiempoInicial));

		// INSERTAMOS PRIMERA POSICION LinkedList
		tiempoInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(0, new Waypoint_Dades(0, "Orbita de la terra", coordenades, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		tiempoFinal = System.nanoTime();
		System.out.println(
				"Temps per insertar 1 waypoint en la 1ra posicio del LinkedList: " + (tiempoFinal - tiempoInicial));

		// INSERTAMOS MITAD POSICION ArrayList
		tiempoInicial = System.nanoTime();
		mitad = comprovacioRendimentTmp.llistaArrayList.size() / 2;
		comprovacioRendimentTmp.llistaArrayList.add(mitad, new Waypoint_Dades(0, "Orbita de la terra", coordenades,
				true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		tiempoFinal = System.nanoTime();
		System.out.println(
				"Temps per insertar 1 waypoint al mig(" + mitad + ")de l'ArrayList " + (tiempoFinal - tiempoInicial));

		// INSERTAMOS MITAD POSICION LinkedList
		tiempoInicial = System.nanoTime();
		mitad = comprovacioRendimentTmp.llistaLinkedList.size() / 2;
		comprovacioRendimentTmp.llistaArrayList.add(mitad, new Waypoint_Dades(0, "Orbita de la terra", coordenades,
				true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		tiempoFinal = System.nanoTime();
		System.out.println(
				"Temps per insertar 1 waypoint al mig(" + mitad + ") del LinkedList " + (tiempoFinal - tiempoInicial));

		// INSERTAMOS POSICIO FINAL ArrayList
		tiempoInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0, "Orbita de la terra", coordenades, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		tiempoFinal = System.nanoTime();
		System.out.println("Temps per insertar 1 waypoint al final de l'ArrayList: " + (tiempoFinal - tiempoInicial));

		// INSERTAMOS POSICION FINAL ArrayList
		tiempoInicial = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0, "Orbita de la terra", coordenades, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
		tiempoFinal = System.nanoTime();
		System.out.println("Temps per insertar 1 waypoint al final del LinkedList: " + (tiempoFinal - tiempoInicial));

		return comprovacioRendimentTmp;
	}

	/*
	 * METODO 4
	 */
	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		int longitud = comprovacioRendimentTmp.llistaArrayList.size();

		System.out.println("LONGITUD!!!!!: " + longitud);

		// APARTADO 1
		List<Integer> idsPerArrayList = new ArrayList<Integer>();
		for (int i = 0; i < longitud; i++) {
			idsPerArrayList.add(i);
		}
		System.out.println("---- APARTAT 1 ----");
		System.out.println("S'ha inicialitzat la llista idsPerArrayList amb " + longitud + " elements.");
		System.out.println("El 1r element t� el valor:" + idsPerArrayList.get(0));
		System.out.println("L'ultim element te el valor: " + idsPerArrayList.get(idsPerArrayList.size() - 1));

		// APARTADO 2
		int i = 0;
		for (Integer integer : idsPerArrayList) {
			System.out.println("ABANS DEL CANVI: comprovacioRendiment.llistaArrayList.get(" + i + ").getId():"
					+ comprovacioRendimentTmp.llistaArrayList.get(i).getId());

			comprovacioRendimentTmp.llistaArrayList.get(i).setId(integer);

			System.out.println("DESPRES DEL CANVI: comprovacioRendiment.llistaArrayList.get(" + i + ").getId():"
					+ comprovacioRendimentTmp.llistaArrayList.get(i).getId());
			System.out.println();
			i++;
		}

		// APARTADO 3
		System.out.println("\n----- APARTAT 3.1 (bucle for) -----");
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println("ID:" + waypoint.getId() + ", nom = " + waypoint.getNom());
		}

		System.out.println("\n----- APARTAT 3.1 (iterator) -----");
		Iterator<Waypoint_Dades> it = comprovacioRendimentTmp.llistaArrayList.iterator();
		Waypoint_Dades waypointIte;
		while (it.hasNext()) {
			waypointIte = it.next();
			System.out.println("ID:" + waypointIte.getId() + ", nom = " + waypointIte.getNom());
		}

		// APARTADO 4
		System.out.println("\n----- APARTAT 4 -----");
		System.out.println("Preparat per esborrar el contingut de llistaLenkedList que te "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements");
		// Borramos
		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.println(
				"Esborrada, Ara llistaLinkedList te " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements");

		// Copiamos/agregamos
		comprovacioRendimentTmp.llistaLinkedList.addAll(comprovacioRendimentTmp.llistaArrayList);
		System.out.println("Copiat els elements de llistaArrayList en llistaLinkedList  que ara te "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements");

		// APARTADO 5
		System.out.println("\n----- APARTAT 5.1 (bucle for) -----");
		// Modificamos
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			if (waypoint.getId() > 5) {
				waypoint.setNom("Orbita de Mart");
				System.out.println("Modificat el waypoint amb id = " + waypoint.getId());
			}
		}

		// Comprobamos
		System.out.println("\n----- APARTAT 5.1 (comprovacio) -----");
		for (int j = 0; j < comprovacioRendimentTmp.llistaArrayList.size(); j++) {
			System.out.println("El waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(j).getId()
					+ " te el nom = " + comprovacioRendimentTmp.llistaArrayList.get(j).getNom());
		}

		System.out.println("\n----- APARTAT 5.2 (Iterator) -----");
		// Modificamos
		Iterator<Waypoint_Dades> iterator = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iterator.hasNext()) {
			waypointIte = iterator.next();
			if (waypointIte.getId() < 5) {
				waypointIte.setNom("Punt lagrange entre la Terra i la Lluna");
				System.out.println("Modificat el waypoint amb id = " + waypointIte.getId());
			}
		}

		System.out.println("\n----- APARTAT 5.2 (comprovacio) -----");
		// Comprobamos
		iterator = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iterator.hasNext()) {
			waypointIte = iterator.next();
			System.out.println("El waypoint amb id = " + waypointIte.getId() + " te el nom = " + waypointIte.getNom());
		}
		return comprovacioRendimentTmp;
	}

	/*
	 * METODO 5
	 */
	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {

		// APARTADO 1
//		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
//			System.out.println("aqui");
//			if (waypoint.getId() < 6) {
//				comprovacioRendimentTmp.llistaArrayList.remove(waypoint);
//			}
//		} -------> Creo que peta por que al eliminar su contenido, el foreach Falla 

		// APARTADO 2
		System.out.println("\n----- APARTAT 2 (Iterator) ----");
		Iterator<Waypoint_Dades> iterator = comprovacioRendimentTmp.llistaLinkedList.iterator();
		Waypoint_Dades waypoint;
		while (iterator.hasNext()) {
			waypoint = iterator.next();
			if (waypoint.getId() > 4) {
				// comprovacioRendimentTmp.llistaArrayList.remove(waypoint);
				iterator.remove();
				System.out.println("Esborrar el waypoint amb id = " + waypoint.getId());
			}
		}

		System.out.println("\n----- APARTAT 2 (comprovacio) ----");
		iterator = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iterator.hasNext()) {
			waypoint = iterator.next();
			System.out.println("El waypoint amb id = " + waypoint.getId() + " te el nom = " + waypoint.getNom());
		}

		// APARTADO 2
		System.out.println("\n----- APARTAT 3(listIterato) -----");
		ListIterator<Waypoint_Dades> it = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		while (it.hasNext()) {
			waypoint = it.next();
			if (waypoint.getId() == 2) {
				it.remove();
				System.out.println("Esborrat el waypoint amb id = " + waypoint.getId());
			}
		}

		System.out.println("\n----- APARTAT 3(comprovacio) ----");
		while (it.hasPrevious()) {
			waypoint = it.previous();
			System.out.println("El waypoint amb id = " + waypoint.getId() + " te el nom = " + waypoint.getNom());
		}
		return comprovacioRendimentTmp;
	}

	/*
	 * METODO 6
	 */
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String nuevasCordenadas = "";

		// Recorremos waypoints
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			if (waypoint.getId() % 2 == 0) {
				System.out.println("----- Modificar el waypoint amb id = " + waypoint.getId() + " -----");

				// Modificamos nombre
				System.out.println("Nom actual: " + waypoint.getNom());
				System.out.println("Nom nou:");
				waypoint.setNom(sc.nextLine());

				// Modificamos coordenadas, tienen que ser 3 elementos separado por 1 espacio en
				// blanco y todos los elementos tienen que ser enteros
				while (nuevasCordenadas.split(" ").length != 3
						|| !Cadena.allParametresInt(nuevasCordenadas.split(" "), 3)) {
					System.out.println("Coordenades actual: " + waypoint.getFormatCoordenades());
					System.out.println("Coordenades noves (format: 1 13 7):");
					nuevasCordenadas = sc.nextLine();
					// Si introduce mas de 3 elementos mostramos el error
					if (nuevasCordenadas.split(" ").length != 3) {
						System.out.println("ERROR: introduir 3 parametres separats per 1 espai en blanc. Has introduit "
								+ nuevasCordenadas.split(" ").length + " parametres");
					} else {
						// Si introduce 3 elementos, comprobamos que todos los elementos sean un entero
						if (Cadena.allParametresInt(nuevasCordenadas.split(" "), 3)) {
							// Convertimos el array de Strings a enteros
							waypoint.setCoordenades(Cadena.arrayStringToInt(nuevasCordenadas.split(" ")));
						} else {
							System.out.println("Error, no son todos enteros");
						}

					}
				}
				System.out.println();
			}
			nuevasCordenadas = "";
		}
		return comprovacioRendimentTmp;
	}

	/*
	 * METODO 6
	 */
	public static void visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println(waypoint);
			System.out.println();
		}
	}

	/*
	 * METODO 7
	 */
	public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String distanciaMaxima = "";

		// Preguntamos hasta que introduzca un digito
		while (!Cadena.stringIsInt(distanciaMaxima)) {
			System.out.println("Distancia maxima de la Terra: ");
			distanciaMaxima = sc.nextLine();
		}

		// Ordenamos y mostramos los waypoint menores a la distnacia introducida
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			if (Waypoint.distancia(waypoint.getCoordenades()) < Integer.parseInt(distanciaMaxima)) {
				System.out.println(waypoint);
			}
		}
	}

	/**
	 * Convertir un array de entero en distancia
	 */
	public static int distancia(int[] cordenadas) {
		int distancia = 0;
		for (int i : cordenadas) {
			distancia += Math.pow(i, 2);
		}
		return distancia;
	}

	//---------------------EXAMEN----------------------------
	/**
	 * Examen
	 * Ejercicio 6
	 * 
	 * Añadimos waypoints a la listaWaypoints
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void InicialitzarDadesWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		if (!comprovacioRendimentTmp.llistaWaypoints.isEmpty()) {
			// Si no esta vacio limpiamos
			comprovacioRendimentTmp.llistaWaypoints.clear();
		} else {
			//añadimos waypoints
			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(0, "Òrbita de la Terra",
					new int[] { 0, 0, 0 }, true, LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna",
					new int[] { 1, 1, 1 }, true, LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 6));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(2, "Òrbita de la LLuna",
					new int[] { 2, 2, 2 }, true, LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 1));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(3, "Òrbita de Mart", new int[] { 3, 3, 3 },
					true, LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(4, "Òrbita de Júpiter",
					new int[] { 4, 4, 4 }, true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa",
					new int[] { 5, 5, 5 }, true, LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 6));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(6, "Òrbita de Europa", new int[] { 6, 6, 6 },
					true, LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));

			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(7, "Òrbita de Venus", new int[] { 7, 7, 7 },
					true, LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null,
					LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));
		}
	}
	
	/**
	 * Examen
	 * Ejercicio 7
	 * 
	 * Insertar un nou waypoint
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void nouWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String nom;
		String coordenades = "";
		boolean activo;
		String dataAnulacio;
		int tipusWaypoint;
		//Nombre
		System.out.println("nom del waypoint:");
		nom = s.nextLine();
		//Cordenadas
		while (!Cadena.allParametresInt(coordenades.split(" "), 3)) {
		System.out.println("Coordenades (format:1 13 7)");
		coordenades = s.nextLine();
			if (coordenades.split(" ").length != 3) {
				System.out.println("Introduir 3 parametres separatas per 1 espai en blanc. Has introduit: " + coordenades.split(" ").length);
			} else {
				System.out.println("Cordenada no valida. El parametres han de ser numeric");
			}
		}		
		
		//Comprobar true or false
		Collator comparador = Collator.getInstance();
		comparador.setStrength(Collator.PRIMARY);
		// Estas dos cadenas son iguales
		//System.out.println(comparador.compare("Hóla", "hola"));
//		while (comparador(activo, true) || comparador(activo, false)) {
//			
//		}  <<-------No me dio tiempo
		
		//////////!!!!!!!!!!!!!!
		// EVA, he llegado hasta aqui, deje este ejercicio para el final por que es el mas largo.
		//Tengo una idea de lo que hay que hacer, pero no me ha dado tiempo
		//Tampoco he tenido tiempo de hacer el ejercicio 11
		//-------------------------------------------------------------------------!!!!!!!!!!!!!!!
		
	}	
	
	/**
	 * Examen
	 * Ejercicio 8
	 * Visualitzar els waypoints versus un tipus.
	 */
	public static void waypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		int opcion;
		System.out.println("Tipus de waypoints disponibles:");
		for (int i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.println(i + ":" + TipusWaypoint.TIPUS_WAYPOINT[i]);
		}	
		System.out.println("Tipus de waypoint (format: 3)");
		opcion = s.nextInt();
		//Creamos listas A y B
		LinkedList<Waypoint_Dades> llistaA = new LinkedList<Waypoint_Dades>();
		LinkedList<Waypoint_Dades> llistaB = new LinkedList<Waypoint_Dades>();
		//Añadimos todos los waypoints a listaA
		llistaA.addAll(comprovacioRendimentTmp.llistaWaypoints);
		//Recorremos la listaA con un interador para poder borrar sin que pete
		ListIterator<Waypoint_Dades> it = llistaA.listIterator();
		Waypoint_Dades waypoint;
		while (it.hasNext()) {
			waypoint = it.next();
			if (waypoint.getTipus() == opcion) {
				llistaB.add(waypoint);
				System.out.println("Esborrat el waypoint amb id = " + waypoint.getId() + " del tipus " + waypoint.getTipus());
				it.remove();
			}
		}	
		System.out.println("\nLlistaB de waypoints del tipus: " + opcion);
		for (Waypoint_Dades waypoints : llistaB) {
			System.out.println("id " + waypoints.getId() + ": " + waypoints.getNom() + ", " + waypoints.getTipus());
		}
	}
	
	/**
	 * Examen
	 * Ejercicio 9
	 * Visualitzar el nº de waypoints versus el seu tipus.
	 */
	public static void numWaypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		//Lista tipo set 
		Set<Waypoint_Dades> waypoints = new HashSet<Waypoint_Dades>();
		//Lista Map tipo treeMap para ordenador por el valor de la llave
		Map<Integer, Integer> mapWaypoint = new TreeMap<Integer, Integer>();
		waypoints.addAll(comprovacioRendimentTmp.llistaWaypoints);
		for (Waypoint_Dades waypoint : waypoints) {
			if (mapWaypoint.containsKey(waypoint.getTipus())) {
				mapWaypoint.put(waypoint.getTipus(), mapWaypoint.get(waypoint.getTipus()) + 1);
			} else {
				mapWaypoint.put(waypoint.getTipus(), 1);
			}
		}
		//Imprimimos
		for (Entry<Integer, Integer> dada : mapWaypoint.entrySet()) {
			System.out.println("Tipus " + dada.getKey() + "(" +TipusWaypoint.TIPUS_WAYPOINT[dada.getKey()] +"):" + dada.getValue() + " naus");		
		}
	}
	
	/**
	 * Examen
	 * Ejercicio 10
	 * Visualitzar el nº de waypoints versus el seu tipus.
	 */
	public static void trobarWaypointVsNom(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String opcion;
		System.out.println("Opcio?");
		opcion = s.nextLine();
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaWaypoints) {	
			//Pasamos a minusculas y comprobamos
			if (waypoint.getNom().toLowerCase().contains(opcion.toLowerCase())) {
				System.out.println("id " + waypoint.getId() + ": " + waypoint.getNom());
			}
		}
	}	
	
	/**
	 * Examen
	 * Ejercicio 10
	 * Visualitzar el nº de waypoints versus el seu tipus.
	 */
	public static void ordenarWaypointPerData() {
		
	}
}
