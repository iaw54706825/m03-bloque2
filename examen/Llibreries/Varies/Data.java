package Varies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import Llibreries.Cadena;

public abstract class Data {
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	/**
	 * Devuelve un LocalDateTime en formato : "dd-MM-yyyy HH:mm"
	 * 
	 * @param date
	 * @return
	 */
	public static LocalDateTime format(String date) {
		return LocalDateTime.parse(date, formatter);
	}

	/**
	 * imprimir Data
	 */
	public static String imprimirData(LocalDateTime dataTmp) {
		if (dataTmp == null) return "NULL";
		return dataTmp.format(formatter);
	}
	
	/**
	 * Examen
	 * 
	 * @param dataTmp
	 * @return
	 */
	public static boolean esData(String dataTmp) {
		//hago un split de la fecha introducido
		String[] data = dataTmp.split("-");
		//Si han insertado tres parametros hago la comprobacion con un metodo propio de la clase Cadena
		if (data.length == 3) {
			return Cadena.allParametresInt(data, 3);		
		}		
		return false;
	}
}
