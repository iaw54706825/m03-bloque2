package streams;

import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.TreeSet;

public class Exercici_4 {

	public static void main(String[] args) {
		// Abrirmos directorio donde trabajaremos
		File d = new File("ex4dir");
		String directorio = "backup";
		// Listamos los archivos y directorios que hay
		File[] listaArchivos = d.listFiles();

		// Ordenamos los directorios por nombre
		TreeSet<String> directorios = new TreeSet<String>();
		for (File s : listaArchivos) {
			if (s.isDirectory() && s.getName().startsWith("backup")) {
				directorios.add(s.getName());
			}
		}
		// Si ya hay directorios de backups creados
		if (directorios.size() > 0) {
			// Sustraemos el numero maximo de la lista de backups
			int numeroBackUps = Integer.parseInt(directorios.last().substring(directorios.last().length() - 1)) + 1;
			// Creamos nuevo backup
			directorio += numeroBackUps;
			File backup = new File("ex4dir/" + directorio);
			backup.mkdir();
		} else {
			File backup = new File("ex4dir/backup0");
			backup.mkdir();
		}

		// Copia de archivos
		for (File archivo : listaArchivos) {
			System.out.println(archivo.getName());
			try {
				InputStream original = new FileInputStream(archivo.getName());
				OutputStream copia = new FileOutputStream("ex4dir/" + archivo.getName());
				try {
					byte data[] = new byte[original.available()];
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
