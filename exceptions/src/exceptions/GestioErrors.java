package exceptions;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class GestioErrors {

	public static void main(String[] args) {
		// 0.1
		Scanner s = new Scanner(System.in);
//		System.out.println("----Entra un id----");
//		Scanner sc = new Scanner(System.in);
//		int user_id = sc.nextInt();
//
//		try {
//			if (user_id != 1234) {
//				throw new InvalidUserIdException();
//			}
//
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//
//		// 0.2
//		int current_balance = 1000;
//		System.out.println("----Entra l� ingr�s a dispositar ----");
//		// Scanner sc = new Scanner(System.in);
//		int deposit_amount = sc.nextInt();
//		try {
//			if (deposit_amount < 0) {
//				throw new NegativeNotAllowedException();
//			} else {
//				current_balance += deposit_amount;
//				System.out.println("Updated balance is: " + current_balance);
//
//			}
//		} catch (Exception e) {
//			System.out.println(e);
//		}

		// Ejercicios Excepciones
		System.out.println("Ejercicio 1");
		// 1.1
		// 1.2 Se puede controlar con un condicional if, dond se controle el length
		// 1.4 Produzco una exepcion y lo capturo
		// 1.5 El codigo que estran dentro del finally siempre se ejecuta, tantas veces
		// como se repita el bloque try/catch
		int[] array = new int[3];
		// Recorremos y el arrat y capturamos la exepcion
		try {
			System.out.println(array[3]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Codi del catch");
			System.out.println("Error de tipo: " + e);
		} finally {
			System.out.println("Final del programa!");
		}

		// 1.6 No captura la exepcion por que no la conoce(?).
		// Lo mejor es captura la clase padre Exception
		System.out.println();
		try {
			System.out.println(array[3]);
		} catch (Exception e) {
			System.out.println("Codi del catch");
			System.out.println("Error de tipo: " + e);
			System.out.println("Propiedades:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println(e.getCause());
			System.out.println(e.getSuppressed());
		} finally {
			System.out.println("Final del programa!");
		}

		System.out.println("\nEjercicio 1.9");
		funcio1();
		s.close();

		System.out.println("--Ejercicio 2--");
		try {
			archivos();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("--Ejercicio 3--");
		divisio();
		
		System.out.println("--Ejercicio 4--");
		//
	}

	
	// Metodos
	// ejercicio 1
	public static void funcio1() {
		System.out.println("funcio 1");
		System.out.println("abans crida funcio2()");
		funcio2();
		System.out.println("despres crida funcio2()");
	}

	public static void funcio2() {
		System.out.println("funcio 2");
		System.out.println("abans crida funcio3()");
		try {
			funcio3();
		} catch (Exception e) {
			System.out.println("Error");
		}
		System.out.println("despres crida funcio3()");
	}

	public static void funcio3() {
		int[] a = new int[1];
		System.out.println(a[1]);
		System.out.println("funcio 3");
	}

	// ejercicio 2
	public static void archivos() throws FileNotFoundException { // Teclado el tipo de esepcion que puede dar
		FileOutputStream f = new FileOutputStream("../docs/test.txt");
		try {
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// ejrcicio 3
	public static void divisio() {
		try {
			int a = 1 / 0;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}


