package exceptions;

public class Exercici5ValidarEdatException extends IllegalArgumentException {

	   @Override
	   public String toString() {
	       return "Edad mayor a 100 o menor que 0";
	   }
	}
