package exceptions;

//Ejercicio 4

//public class Persona {
//	private int edat;
//
//	public void setEdat(int edat) {
//		try {
//			if (edat < 0) {
//				throw new IllegalArgumentException();
//			}
//
//		} catch (IllegalArgumentException e) {
//			System.out.println(e);
//		}
//	}
//	public static void main(String[] args) {
//		Persona p = new Persona ();
//		p.setEdat(-1);
//	}
//}

//Ejercicio 5
public class Persona {
	private int edat;

	public void setEdat(int edat) {
		try {
			if (edat < 0 || edat > 100) {
				throw new Exercici5ValidarEdatException();
			}

		} catch (Exercici5ValidarEdatException e) {
			System.out.println(e);
		}
	}
	public static void main(String[] args) {
		Persona p = new Persona ();
		p.setEdat(-1);
	}
}

