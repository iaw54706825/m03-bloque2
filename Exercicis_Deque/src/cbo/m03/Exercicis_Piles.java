package cbo.m03;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class Exercicis_Piles {

	public static void main(String[] args) {
		System.out.println("----- Ejercicio 1 -----");
		Deque<Integer> p1 = new ArrayDeque<Integer>();
		p1.add(1);
		p1.add(2);
		p1.add(3);
		p1.add(4);
		System.out.println(p1);
		// invertirPila(p1);
		invertirPilaGeneric(p1);

		System.out.println("\n----- Ejercicio 2 -----");
		Deque<String> mina = new ArrayDeque<String>();
		mina.add("<");
		mina.add(">");
		mina.add("(");
		mina.add("<");
		mina.add(">");
		mina.add(")");
		mina.add("<");
		mina.add("(");
		mina.add("<");
		mina.add(")");
		System.out.println(mina);
		diamants(mina);

		System.out.println("\n----- Ejercicio 3 -----");
		comprovador("25+3*(1+2+(30*4))/2");

		System.out.println("\n----- Ejercicio 4 -----");
		Deque<String> cua1 = new LinkedList<String>();
		Deque<String> cua2 = new ArrayDeque<>();
		cua1.addAll(Arrays.asList("1", "2", "3", "4"));
		cua2.addAll(Arrays.asList("1", "2", "3", "4"));
		reverseG(cua1);// Generic
		reverse(cua2);// Normal
	}

	/**
	 * Invertir una pila sin elementos genericos *
	 * 
	 * @param pila
	 */
	public static void invertirPila(Deque<Integer> pila) {
		Deque<Integer> pilaInvertida = new ArrayDeque<Integer>();
		while (!pila.isEmpty()) {
			pilaInvertida.add(pila.pollLast());
		}
		// while(!pila.isEmpty()) {
		// pilaInvertida.push(pila.pop());
		// }
		System.out.println("Pila no generica: " + pilaInvertida);
	}

	/**
	 * Invertir una pila con elementos genericos *
	 * 
	 * @param pila
	 */
	public static <T> void invertirPilaGeneric(Deque<T> pila) {
		Deque<T> pilaInvertida = new ArrayDeque<T>();
		while (!pila.isEmpty()) {
			pilaInvertida.add(pila.pollLast());
		}
		System.out.println("Pila generica :" + pilaInvertida);
	}

	/**
	 * Ejercicio2
	 * 
	 * @param mina
	 */
	public static void diamants(Deque<String> mina) {
		Deque<String> pilaDiamants = new ArrayDeque<String>();
		int diamantBlanc = 0;
		int duamantNegre = 0;

		for (String elem : mina) {
			if (!pilaDiamants.isEmpty() && pilaDiamants.peekFirst().equals("<") && elem.equals(">")) {
				diamantBlanc++;
				pilaDiamants.pop();
			} else {
				if (!pilaDiamants.isEmpty() && pilaDiamants.peekFirst().equals("(") && elem.equals(")")) {
					duamantNegre++;
					pilaDiamants.pop();
				} else
					pilaDiamants.push(elem);
			}
		}
		System.out.println("Diamants blancs: " + diamantBlanc);
		System.out.println("Diamants negres: " + duamantNegre);
	}

	/**
	 * Ejercicio3 Verefica expresion matematica
	 */
	public static void comprovador(String expresion) {
		Deque<String> cua = new ArrayDeque<String>();
		Deque<String> pila = new ArrayDeque<String>();
		for (int i = 0; i < expresion.length(); i++) {
			if (expresion.charAt(i) == ')' || expresion.charAt(i) == '(') {
				cua.add(expresion.charAt(i) + "");
			}
		}
		System.out.println("Cua: " + cua);
		for (String string : cua) {
			if (string.equals("(")) {
				pila.add(string);
			} else {
				pila.remove();
			}
		}
		System.out.println("Pila: " + pila);
	}

	/**
	 * Ejercicio 4 Reverse
	 */
	public static void reverse(Deque<String> cua) {
		Deque<String> pila = new ArrayDeque();
		Iterator<String> it = cua.iterator();
		while (it.hasNext()) {
			pila.push(it.next());
		}
		System.out.println("Iterator: " + pila);
		pila.clear();
		for (String string : cua) {
			pila.push(string);
		}
		System.out.println("ForEach: " + pila);
	}

	/**
	 * Ejercicio 4 Reverse generic
	 */
	public static <T> void reverseG(Deque<T> cua) {
		Deque<T> pila = new ArrayDeque();
		Iterator<T> it = cua.iterator();
		while (it.hasNext()) {
			pila.push(it.next());
		}
		System.out.println("Iterator: " + pila);
		pila.clear();
		for (T string : cua) {
			pila.push(string);
		}
		System.out.println("ForEach: " + pila);
	}
}
