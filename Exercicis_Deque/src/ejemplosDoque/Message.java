package ejemplosDoque;

public interface Message<T> {

	T displayMessage(T message);
}
