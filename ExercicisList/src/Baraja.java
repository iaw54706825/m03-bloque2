import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baraja {
	private String pals[] = {"cors","piques","diamants","trevols"};
	private String cartas[] = {"As","2","3","4","5","6","7","8","9","10","J","Q","K"};
	private List<String> listaBaraja = new ArrayList<String>();
	
	//Contruimos la baraja	
	public void construirBaraja() {
		for (int e = 0; e < pals.length; e++) {
			for (int j = 0; j < cartas.length; j++) {
				listaBaraja.add(pals[e] + cartas[j]);
			}			
		}	
	}
	
	/**
	 * Metodo para ver la bajara
	 */
	public void verBaraja() {
		for (String string : listaBaraja) {
			System.out.println(string);
		}
	}	
	
	
	/**
	 * @return the listaBaraja
	 */
	public List<String> getListaBaraja() {
		return listaBaraja;
	}

	/**
	 * Metodo para repartir
	 * @param baraja numero de cartas
	 * @param numCartas numero de jugadores
	 * return mano
	 */	
	public static List<String> repartir(List<String> baraja, int numCartas){
		//removemos las cartas
		Collections.shuffle(baraja);
		//A�adimos las ultimas segun numCartas
		List<String> subList = baraja.subList(baraja.size()-numCartas,baraja.size());
		List<String> mano = new ArrayList<String>(subList);
		//Removemos las ultimas
		baraja.subList(baraja.size()-numCartas,baraja.size()).clear();
		return mano;
		}
}
