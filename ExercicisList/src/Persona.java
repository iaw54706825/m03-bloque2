import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Persona implements Comparable<Persona> {
	private String nom;
	private String cognoms;
	private LocalDate naixement; // Formato dd-mm-yyy
	private String dni;

	/**
	 * @param nom
	 * @param cognoms
	 * @param naixement
	 * @param dni
	 */
	public Persona(String nom, String cognoms, String naixement, String dni) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		this.nom = nom;
		this.cognoms = cognoms;
		this.naixement = LocalDate.parse(naixement, formatter);
		this.dni = dni;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return the naixement
	 */
	public LocalDate getNaixement() {
		return naixement;
	}

	/**
	 * @param naixement
	 * @return age
	 */
	public int getAge() {
		LocalDate hoy = LocalDate.now();
		// Creamos un periodo entre las dos fechas
		Period edad = Period.between(this.naixement, hoy);

		return edad.getYears();
	}
	
	
	/**
	 * Implementamos el metodo compareTo.
	 */
	@Override
	public int compareTo(Persona o) {
		//Si nacieron el mismo dia, comparamos por nombre
		if (this.naixement.compareTo(o.naixement) == 0) {
			return this.nom.compareTo(o.nom);
		}
		return o.naixement.compareTo(this.naixement);
	}

	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", naixement=" + naixement + "]";
	}

	/**
	 * Metodo para comprobar si una persona es menor de edad, y devolver una lista
	 * de estas Este metodo calcula la edad a partir del metodo getAge() de la clase Persona 
	 * @param listaPersonas
	 * @return
	 */
	public static ArrayList<Persona> menorsEdaT(List<Persona> listaPersonas) {
		ArrayList<Persona> menores = new ArrayList<Persona>();
		for (Persona persona : listaPersonas) {
			if (persona.getAge() < 18) {
				menores.add(persona);
			}
		}
		return menores;
	}
}
