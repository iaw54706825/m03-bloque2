
public class ContactoAgenda implements Comparable <ContactoAgenda>{
	private String nom;
	private String cognom1;
	private String cognom2;
	private String telefono;
	private String email;
	private String movil;
	
	/**
	 * @param nom
	 * @param cognom1
	 * @param cognom2
	 * @param telefono
	 * @param email
	 * @param movil
	 */
	public ContactoAgenda(String nom, String cognom1, String cognom2, String telefono, String email, String movil) {
		this.nom = nom;
		this.cognom1 = cognom1;
		this.cognom2 = cognom2;
		this.telefono = telefono;
		this.email = email;
		this.movil = movil;
	}

	/**
	 * @param nom
	 * @param cognom1
	 * @param cognom2
	 */
	public ContactoAgenda(String nom, String cognom1, String cognom2) {
		this.nom = nom;
		this.cognom1 = cognom1;
		this.cognom2 = cognom2;
	}

	@Override
	public int compareTo(ContactoAgenda o) {
		if (this.cognom1.compareTo(o.cognom1) == 0) {
			if (this.cognom2.compareTo(o.cognom2) == 0) {
				return this.nom.compareTo(o.nom);
			} else {
				return this.cognom2.compareTo(o.cognom2);
			}
		}
		return cognom1.compareTo(o.cognom1);
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "ContactoAgenda [nom=" + nom + ", cognom1=" + cognom1 + ", cognom2=" + cognom2 + "]";
	}		
	
	
	
	
}