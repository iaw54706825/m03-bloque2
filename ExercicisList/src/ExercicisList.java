import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


public class ExercicisList {	
	
	
	public static void main(String[] args) {
		
		System.out.println("---------EJERCICIO_1----------\n");
		
		//Creamos un ArrayList
		List<Integer> l1 = new ArrayList<Integer>();
	
		//Pasamos a enteros
		for (int i = 0; i < args.length; i++) {
			l1.add(Integer.parseInt(args[i]));
		}
			
		//Imprimimos los elementos a partir del su metodo size() y elevamos al cudrado
		System.out.println("Imprimimor a partir del metodo size()-------");
		System.out.println("Y elevamos al cuadrado con Math.pow-------");
		int length = l1.size();
		for (int i = 0; i < length; i++) {
			//Muestro el valor original
			System.out.println("Valor: " + l1.get(i));
			
			//Muestro el valor establecido al cuadrado
			l1.set(i, (int)Math.pow(l1.get(i), 2));
			System.out.println("Valor al cuadrado: " + l1.get(i));
		}
		
		System.out.println();
		//Con iterator condicionamos
		//System.out.println("-------Usamos Iterator para condicionar que los numeros sean menores a 100-------");
		Iterator<Integer> it = l1.iterator();
		while (it.hasNext()) {
			if (it.next() > 100) {
				it.remove();
			}
		}
		
		//Ordenamos
		System.out.println("Ordenamos con Collections.sort()-------");
		Collections.sort(l1);		
		for (Integer integer : l1) {
			System.out.println(integer);
		}		
		
		
		System.out.println("\n---------EJERCICIO_2----------\n");		
		ArrayList<Integer> l2 = new ArrayList<Integer>();
		l2.add(10);
		l2.add(20);
		l2.add(30);
		l2.add(40);
		
		l1.addAll(l2);
		System.out.println("Concatenamos l1 y l2");
		for (Integer integer : l1) {			
			System.out.println(integer);
		}
		
		System.out.println("\nComprobamos que l1 contiene los elementos de l2 con contains()");
		for (Integer integer : l2) {			
			System.out.println(l1.contains(integer));			
		}
		
		//Eliminamos contenido lista e imprimimos
		l2.removeAll(l2);
		System.out.println("Esta vacio (l2.isEmpty):  " + l2.isEmpty());
				
		
		System.out.println("\n---------EJERCICIO_3----------\n");
		//Creamos una lista de personas. Fecha nacimiento formato: dd-mm-yyyy
		Persona p1 = new Persona("Carlos", "Bernuy", "25-09-1991", "2535947z");
		Persona p2 = new Persona("Cindy", "Beltran", "30-06-2004", "3548971x");
		Persona p3 = new Persona("Pedro", "Lopez", "10-07-1992", "9761812p");
		Persona p4 = new Persona("Katty", "Torres", "30-06-2004", "3154986t");
		List<Persona> listaPersonas = new ArrayList<Persona>();
		listaPersonas.add(p1);
		listaPersonas.add(p2);
		listaPersonas.add(p3);
		listaPersonas.add(p4);
		
		/*Utilizamos el metodo static de este clase, para calcular si una persona
		 *es menor de edad. Este metodo nos devuelve un List<Persona>.
		 *Aqui no estoy seguro donde crear el metodo, si en esta clase, o en la clase Persona
		 */	
		System.out.println("Personas menores de 18 a�os:");
		List<Persona> menores = Persona.menorsEdaT(listaPersonas); //Se puede poner el nombre de la clase antes del metodo.
		System.out.println(menores); //Podria imprimir directamente por que he sobreescrito el toString
		
		
		System.out.println("\n---------EJERCICIO_4----------\n");
		/*Ordenamos la lista de Personas, para esto en la clase Persona
		 *implementamos la interface Comparable.
		 *En este ejercicio haciendo un while de una lista ordenada, no
		 *es necesario recorrer todo el List 
		 */
		Collections.sort(listaPersonas);
		int i = 0;
		while (listaPersonas.get(i).getAge() < 18) {
			if (listaPersonas.get(i).getAge() < 18) {
				System.out.println(listaPersonas.get(i));
				i++;
			} else {
				break;
			}
		}
		
		System.out.println("\n---------EJERCICIO_5----------\n");
		//NO es necesario volver a ordenalo, ya lo hice en el ejercicio anterior
		Collections.sort(listaPersonas);
		//System.out.println("-------Usamos Iterator para imprimir-------");
		Iterator<Persona> itp = listaPersonas.iterator();
		while (itp.hasNext()) {
			System.out.println(itp.next());
		}
		

		System.out.println("\n---------EJERCICIO_6----------\n");
		ContactoAgenda c1 = new ContactoAgenda("Carlos","Bernuy","Obregon");
		ContactoAgenda c2 = new ContactoAgenda("Cindy","Beltran","Cardenas");
		ContactoAgenda c3 = new ContactoAgenda("Juan","Astro","Pia");
		ArrayList<ContactoAgenda> agenda = new ArrayList<ContactoAgenda>();
		agenda.add(c1);
		agenda.add(c2);
		agenda.add(c3);
		System.out.println("Agenda sin ordenar:");
		for (ContactoAgenda contactoAgenda : agenda) {
			System.out.println(contactoAgenda);
		}
		System.out.println("\nAgenda ordenada:");
		Collections.sort(agenda);
		for (ContactoAgenda contactoAgenda : agenda) {
			System.out.println(contactoAgenda);
		}
		

		System.out.println("\n---------EJERCICIO_7----------\n");
		/* En este apartado he creado la clase baraja
		 * 1ro instanciamos
		 * 2do luego construimos la lista de cartas de tipo americana
		 * Implemente metodos que podrian ser necesarios.
		 */
		Baraja b = new Baraja();
		b.construirBaraja();
		b.verBaraja();
		ArrayList<String> cartas = new ArrayList<String>(Baraja.repartir(b.getListaBaraja(),6));
		System.out.println(cartas);
	}
}
