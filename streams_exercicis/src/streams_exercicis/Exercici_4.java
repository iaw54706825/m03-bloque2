package streams_exercicis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.TreeSet;

public class Exercici_4 {

	public static void main(String[] args) {
		// Abrirmos directorio donde trabajaremos
		File d = new File("ex4");
		String directorio = "backup";
		// Listamos los archivos y directorios que hay
		File[] listaArchivos = d.listFiles();	
	
		// Ordenamos los directorios por nombre y seleccionamos los directorios
		TreeSet<String> directorios = new TreeSet<String>();
		for (File s : listaArchivos) {
			if (s.isDirectory() && s.getName().startsWith("backup")) {
				directorios.add(s.getName());
			}
		}
		// Si ya hay directorios de backups creados
		if (directorios.size() > 0) {
			// Sustraemos el numero maximo de la lista de backups
			int numeroBackUps = Integer.parseInt(directorios.last().substring(directorios.last().length() - 1)) + 1;
			// Creamos nuevo backup
			directorio += numeroBackUps;
			File backup = new File("ex4/" + directorio);
			directorio = backup.toString();
			backup.mkdir();
		} else {
			File backup = new File("ex4/backup0");
			directorio = backup.toString();
			backup.mkdir();
		}
		//Copia de seguridad
		for (File archivo : listaArchivos) {
			//Si es no es un directorio y se puede leer
			if (!archivo.isDirectory() && archivo.canRead()) {
				try {
					//Preparamos la lectura del achivo original(con su ruta)
					InputStream archivoOriginal = new FileInputStream(archivo.toString());
					//Indicamos donde ira la copia del archivo original(con su ruta)
					//System.out.println(directorio);
					OutputStream archivoCopia = new FileOutputStream(directorio + "/"+ archivo.getName());
					try {
						//Preparamos la copia
						byte data[] = new byte[archivoOriginal.available()];
						archivoOriginal.read(data, 0, archivoOriginal.available());
						//Escribimos todo
						archivoCopia.write(data);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			if (!archivo.isDirectory() && !archivo.canRead()) {
				System.out.println("El archivo no se puede leer");
			}
			
		}
		
	}

}
