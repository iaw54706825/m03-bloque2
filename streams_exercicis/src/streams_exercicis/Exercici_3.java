package streams_exercicis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercici_3 {

//	Escriu un programa que compari dos arxius de text l�nia per l�nia. 
//	S'ha de llegir una l�nia de cada arxiu i comparar-les. Si aquestes no coincideixen, s'ha de mostrar  el n�mero de l�nia que falla i les dues l�nies que s�n diferents. El programa processar� totes les l�nies de l'arxiu. 
//	Fes servir els Readers / Writers (apartat 1.3. dels apunts) per a llegir i escriure en fitxer.
	public static void main(String[] args) {
		try {
			BufferedReader a1 = new BufferedReader(new FileReader("ex3a.txt"));
			BufferedReader a2 = new BufferedReader(new FileReader("ex3b.txt"));
			int pos = 0;
			String linea1;
			String linea2;
			while((linea1 = a1.readLine()) != null && (linea2 = a2.readLine()) != null) {
				if (!linea1.equals(linea2)) {
					System.out.println("Lineas diferentes, linea: " + pos);
					System.out.println(linea1);
					System.out.println(linea2);
				}
				pos++;
			}
			System.out.println("final lectura");
			a1.close();
			a2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("El fichero no existe");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} 
	}	
}
