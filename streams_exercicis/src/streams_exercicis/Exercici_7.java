package streams_exercicis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Exercici_7 {
	public static void main(String[] args) {
		// Guardara los caracteres
		Map<String, Integer> caracteres = new HashMap<String, Integer>();
		try {
			// Prefaramos fichero
			BufferedReader fichero = new BufferedReader(new FileReader("ex7/ej7.txt"));
			String linea;
			while ((linea = fichero.readLine()) != null) {
				// Convertimos cada linea en un array de palabras, omitiendo todos los espacios
				// en blanco
				String[] palabras = linea.toString().split("\\s+");
				// recorremos el array de palavras
				for (String palabra : palabras) {
					// Recorremos cada palabras con .charAt()
					for (int i = 0; i < palabra.length(); i++) {
						// Condicionamos
						if (!caracteres.containsKey(palabra.charAt(i) + "")) {
							caracteres.put(palabra.toLowerCase().charAt(i) + "", 1);
						} else {
							caracteres.put(palabra.toLowerCase().charAt(i) + "",
									caracteres.get(palabra.toLowerCase().charAt(i) + "") + 1);
						}
					}
				}
			}
			//Escribimos los caracteres
			registroCaracteres(caracteres);
			fichero.close();
		} catch (FileNotFoundException e) {
			System.out.println("fichero no encontrado");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	private static void registroCaracteres(Map<String, Integer> registre) throws IOException {
		FileWriter fwriter = null;
		try {
			fwriter = new FileWriter(new File("ex7/registroCaracteres.txt"));
			Set<String> values = registre.keySet();		
			for (String paraula : values) {
				fwriter.write(paraula + ": " + registre.get(paraula).toString() + " vegades\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			fwriter.close();
		}
	}
}
