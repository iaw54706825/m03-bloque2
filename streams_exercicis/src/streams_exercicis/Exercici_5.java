package streams_exercicis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercici_5 {

	public static void main(String[] args) {
		int contadorLineas = 0;
		int contadorCaracteres = 0;
		File directorio = new File("ex5");
		File[] listaArchivos = directorio.listFiles();
		for (File file : listaArchivos) {
			try {
				// Por lineas
				BufferedReader lecturaLineas = new BufferedReader(new FileReader(file));
				String linea;
				while ((linea = lecturaLineas.readLine()) != null) {
					contadorLineas++;
				}

				// Por caracteres
				BufferedReader lecturaCaracteres = new BufferedReader(new FileReader(file));
				int caracteres;
				while ((caracteres = lecturaCaracteres.read()) != -1) {
					contadorCaracteres++;
				}
				System.out.println("Nombre: " + file.getName() + " - Lineas: " + contadorLineas + " - Caracteres:"
						+ contadorCaracteres);
				contadorLineas = 0;
				contadorCaracteres = 0;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
