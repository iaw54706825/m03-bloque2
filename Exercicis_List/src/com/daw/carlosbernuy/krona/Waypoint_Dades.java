package com.daw.carlosbernuy.krona;

import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

public class Waypoint_Dades implements Comparable<Waypoint_Dades>{
	private int id;                     
	private String nom;
	private int[] coordenades;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
	
	/**
	 * 
	 * @param id
	 * @param nom
	 * @param coordenades
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	// ---- toString
	@Override
	public String toString() {
		String datacreacio1;
		String dataAnulacio1;
		String dataModificacio1;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		
		if (dataCreacio != null) {
			datacreacio1 = dataCreacio.format(formatter);
		} else {
			datacreacio1 = "NULL";
		}
		/////////////
		if (dataAnulacio != null) {
			dataAnulacio1 = dataAnulacio.format(formatter);
		} else {
			dataAnulacio1 = "NULL";
		}
		////////////
		if (dataModificacio != null) {
			dataModificacio1 = dataModificacio.format(formatter);
		} else {
			dataModificacio1 = "NULL";
		}
		return "WAYPOINT " +  id + 
				"\n  nom= " + nom																	//Metodo propio 																					
				+ "\n  coordenades(x, y, z) = " + Arrays.toString(coordenades) + " (distancia = " + Waypoint.distancia(this.coordenades) +")"
				+ "\n  actiu = " + actiu 
				+ "\n  dataCreacio = " + datacreacio1
				+ "\n  dataAnulacio = " + dataAnulacio1
				+ "\n  dataModificacio = " + dataModificacio1 ;
	}	


	@Override
	public int compareTo(Waypoint_Dades o) {
		int Tdistancia = Waypoint.distancia(this.coordenades);
		int Odistancia = Waypoint.distancia(o.coordenades);		
		if (Tdistancia == Odistancia ) {
			Collator collator = Collator.getInstance(new Locale("es"));
			collator.setStrength(Collator.PRIMARY);
			return collator.compare(this.nom, o.nom);
		} 
		return Tdistancia - Odistancia;
	}

	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the coordenades
	 */
	public int[] getCoordenades() {
		return coordenades;
	}
	
	/**
	 * @return coordenadas con formato
	 */
	public String getFormatCoordenades() {
		return this.coordenades[0] + " " + this.coordenades[1] + " " +this.coordenades[2];
	}

	/**
	 * @param coordenades the coordenades to set
	 */
	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}

	/**
	 * @return the actiu
	 */
	public boolean isActiu() {
		return actiu;
	}

	/**
	 * @param actiu the actiu to set
	 */
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	/**
	 * @return the dataCreacio
	 */
	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	/**
	 * @return the dataAnulacio
	 */
	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	/**
	 * @param dataAnulacio the dataAnulacio to set
	 */
	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}

	/**
	 * @return the dataModificacio
	 */
	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}

	/**
	 * @param dataModificacio the dataModificacio to set
	 */
	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}    			
	
	
}
