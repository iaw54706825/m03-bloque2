package Llibreries;

public class Cadena {
	
	public static boolean stringIsInt(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	
	public static boolean allParametresInt(String[] array) {
		int i = 0;
		while (i < array.length && Cadena.stringIsInt(array[i]) ) {
			i++;
		} 
		return i == 3;
	}
	
	public static int[] arrayStringToInt(String[] array) {
		int[] nuevoArray = new int[array.length];
		for(int i = 0; i < array.length; i++) {
			nuevoArray[i] = Integer.parseInt(array[i]); 
		}
		return nuevoArray;
	}
}
