package Exercici_refor_de_1r;

import java.time.LocalDateTime;

/**
 * @author Carlos
 *
 */
public class Mariner extends Tripulant {
	private boolean serveiEnElPont;
	private String descripcoiFeina;

	/**
	 * 
	 */
	public Mariner() {
		// TODO Ap�ndice de constructor generado autom�ticamente
	}	

	/**
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 * @param serveiEnElPont
	 * @param descripcoiFeina
	 */
	public Mariner(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcoiFeina) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcoiFeina = descripcoiFeina;
	}




	@Override
	protected void ImprimirDadesTripulant() {
		System.out.println("Bandol:" + bandol);
		System.out.println("ID:" + this.ID);
		System.out.println("Nom:" + this.nom);
		System.out.println("Actiu:" + this.actiu);
		System.out.println("Departament (de la classe Tripulant):" + this.departament);
		System.out.println("Departament (de la classe IKSRotarranConstants):" + IKSRotarranConstants.DEPARTAMENT[this.departament]);
		System.out.println("Lloc de servei (de la classe Tripulant):" + this.getLlocDeServei());
		System.out.println("Lloc de servei (de la classe IKSRotarranConstants):" + IKSRotarranConstants.LLOCS_DE_SERVEI[this.getLlocDeServei()]);
		System.out.println("Descripcio de la feina que fa:" + this.descripcoiFeina);
		System.out.println("Serveixen el pont?:" + this.serveiEnElPont);
		System.out.println("Data d'alta:" + this.dataAlta);

	}

}
