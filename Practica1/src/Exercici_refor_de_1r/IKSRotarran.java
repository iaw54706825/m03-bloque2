/**
 * 
 */
package Exercici_refor_de_1r;

import java.time.LocalDateTime;

/**
 * @author Carlos
 *
 */
public class IKSRotarran {
	
	/**
	 * Formateador LocalDateTime, Formatea un String (DD-MM-YYYY HH:MM) a LocalDateTime 
	 */
	public static LocalDateTime formateador(String fecha) {
		String year = fecha.substring(6, 10);
		String mes = fecha.substring(3, 5);
		String dia = fecha.substring(0, 2);
		String hora = fecha.substring(11, 13);
		String minuto = fecha.substring(14);

		if (mes.indexOf("0") != -1) {
			mes = mes.substring(1);
		}
		if (dia.indexOf("0") != -1) {
			dia = dia.substring(1);
		}
		if (hora.indexOf("0") != -1) {
			hora = hora.substring(1);
		}
		if (minuto.indexOf("0") != -1) {
			minuto = minuto.substring(1);
		}
		return LocalDateTime.of(Integer.parseInt(year), Integer.parseInt(mes), Integer.parseInt(dia),
				Integer.parseInt(hora), Integer.parseInt(minuto));
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Creamos un capitan
		Oficial capita = new Oficial("001-A", "Martok", true, IKSRotarran.formateador("15-08-1954 00:01"), 1, 1, true, "Capitanejar la nau");

		// Creamos un marinero
		Mariner mariner_02_03 = new Mariner("758-J", "Kurak", true, IKSRotarran.formateador("26-12-1981 13:42"), 3, 1, true,
				"Mariner encarregat del tim� i la navegaci� durant el 2n torn");
		
		//Obtenemos departamento
		System.out.println(capita.departament);
		//Obtener descripcion trabajo Atributo
		//System.out.println(capita.descripcoiFeina);
		//Obtener descripcion trabajo Getter
		System.out.println(capita.getDescripcoiFeina());
		
		//Imprimimos datos del capitan
		capita.ImprimirDadesTripulant();
		
		//Cambiamos numero departamento capitan accediento al atributo
		capita.departament = 10;
		
		//
		Tripulant oficialDeTipusTripulant = new Oficial();
		Oficial oficialDeTipusOficial = new Oficial();
		
		oficialDeTipusTripulant.saludar();
		oficialDeTipusOficial.saludar();

		
		//
		System.out.println("L'objecte capita (t� implementat el toString()): " + capita);
		System.out.println("L'objecte mariner_02_03 (NO t� implementat el toString()): " + mariner_02_03);
		
		System.out.println();
		mariner_02_03.ImprimirDadesTripulant();
	}
}
