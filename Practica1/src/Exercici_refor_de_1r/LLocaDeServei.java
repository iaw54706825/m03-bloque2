package Exercici_refor_de_1r;

/**
 * @author Carlos
 *
 */
public class LLocaDeServei {
	private int ID;
	private String nom;
	private String descripcio;

	/**
	 * @param iD
	 * @param nom
	 * @param descripcio
	 */
	public LLocaDeServei(int iD, String nom, String descripcio) {
		super();
		ID = iD;
		this.nom = nom;
		this.descripcio = descripcio;
	}

	/**
	 * @return el iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param iD el iD a establecer
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * @return el nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom el nom a establecer
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return el descripcio
	 */
	public String getDescripcio() {
		return descripcio;
	}

	/**
	 * @param descripcio el descripcio a establecer
	 */
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

}
