package Exercici_refor_de_1r;

import java.time.LocalDateTime;

/**
 * @author Carlos
 *
 */
public class Oficial extends Tripulant {
	private boolean serveiEnElPont;
	private String descripcoiFeina;


	/**
	 * 
	 */
	public Oficial() {
	}
	
	/**
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param serveiEnElPont
	 * @param descripcoiFeina
	 */
	public Oficial(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcoiFeina) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcoiFeina = descripcoiFeina;
	}

	/**
	 * @return el serveiEnElPont
	 */
	public boolean isServeiEnElPont() {
		return serveiEnElPont;
	}

	/**
	 * @param serveiEnElPont el serveiEnElPont a establecer
	 */
	public void setServeiEnElPont(boolean serveiEnElPont) {
		this.serveiEnElPont = serveiEnElPont;
	}

	/**
	 * @return el descripcoiFeina
	 */
	public String getDescripcoiFeina() {
		return descripcoiFeina;
	}

	/**
	 * @param descripcoiFeina el descripcoiFeina a establecer
	 */
	public void setDescripcoiFeina(String descripcoiFeina) {
		this.descripcoiFeina = descripcoiFeina;
	}


	@Override
	public String toString() {
		return "Oficial [serveiEnElPont=" + serveiEnElPont + ", descripcoiFeina=" + descripcoiFeina + ", ID=" + ID
				+ ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament=" + departament + "]";
	}
	
	// Metodos
	
	/**
	 *
	 */
	@Override
	protected void ImprimirDadesTripulant() {
		System.out.println(this);
	}

	/**
	 *
	 */
	private boolean serveixEnElPont() {
		return this.serveiEnElPont;
	}
	
	/*
	 * Metodo saludar
	 */
	@Override
	public void saludar () {
		System.out.println("Hola des de la subclasse Oficial");
	}

}
