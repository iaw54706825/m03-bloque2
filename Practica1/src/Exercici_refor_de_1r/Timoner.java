package Exercici_refor_de_1r;

import java.time.LocalDateTime;

/**
 * @author Carlos
 *
 */
public class Timoner extends Tripulant {
	private boolean serveiEnElPont;
	private String descripcoiFeina;

	/**
	 * 
	 */
	public Timoner() {
		// TODO Ap�ndice de constructor generado autom�ticamente
	}	

	/**
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 * @param serveiEnElPont
	 * @param descripcoiFeina
	 */
	public Timoner(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcoiFeina) {
		super(iD, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcoiFeina = descripcoiFeina;
	}




	@Override
	protected void ImprimirDadesTripulant() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
	}

}
