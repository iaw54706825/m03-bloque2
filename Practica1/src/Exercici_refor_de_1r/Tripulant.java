package Exercici_refor_de_1r;

import java.time.LocalDateTime;

public abstract class Tripulant {
	protected static final String bandol = "Imperi Klingon";
	protected String ID;
	protected String nom;
	protected boolean actiu;
	protected LocalDateTime dataAlta;
	protected int departament;
	private int llocDeServei;
	
	

	/**
	 * 
	 */
	public Tripulant() {
	}

	/**
	 * @param iD
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 */
	public Tripulant(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei) {
		ID = iD;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.departament = departament;
		this.llocDeServei = llocDeServei;
	}

	/**
	 * @return el iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD el iD a establecer
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return el nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom el nom a establecer
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return el actiu
	 */
	public boolean isActiu() {
		return actiu;
	}

	/**
	 * @param actiu el actiu a establecer
	 */
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	/**
	 * @return el dataAlta
	 */
	public LocalDateTime getDataAlta() {
		return dataAlta;
	}

	/**
	 * @param dataAlta el dataAlta a establecer
	 */
	public void setDataAlta(LocalDateTime dataAlta) {
		this.dataAlta = dataAlta;
	}

	/**
	 * @return el departament
	 */
	public int getDepartament() {
		return departament;
	}

	/**
	 * @param departament el departament a establecer
	 */
	public void setDepartament(int departament) {
		this.departament = departament;
	}

	/**
	 * @return el llocDeServei
	 */
	public int getLlocDeServei() {
		return llocDeServei;
	}

	/**
	 * @param llocDeServei el llocDeServei a establecer
	 */
	public void setLlocDeServei(int llocDeServei) {
		this.llocDeServei = llocDeServei;
	}

	/**
	 * @return el bandol
	 */
	public static String getBandol() {
		return bandol;
	}		

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}

	
	//Metodos

	/*
	 * Method abstract
	 */
	protected abstract void ImprimirDadesTripulant();
	
	
	/*
	 * Metodo saludar
	 */
	public void saludar () {
		System.out.println("Hola des de la superclasse Tripulant");
	}
}
