
public class Car implements Vehicle{
	private String nom;
	private double preu;
	private String marca;
	
	public Car(String nom, double preu, String marca) {
		this.nom = nom;
		this.preu = preu;
		this.marca = marca;
	}

	//GETTERS & SETTERS
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public void Drive() {
		System.out.println("Conduciendo el coche");
	}

	@Override
	public void Stop() {
		System.out.println("Parando el coche");
	}

	@Override
	public void StartEngine() {
		System.out.println("Arrancando motor");		
	}

	@Override
	public void Turn() {
		System.out.println("Girando");
		
	}

	@Override
	public void GotoITV() {
		System.out.println("Pasando ITV");		
	}

	@Override
	public String toString() {
		return "Car [nom=" + nom + ", preu=" + preu + ", marca=" + marca + "]";
	}
	
	

	
}
