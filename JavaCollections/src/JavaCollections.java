import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {
		
		
		ArrayList<String> l1 = new ArrayList<String>();
		//Agregar
		l1.add("gat");
		l1.add("jaguar");
		l1.add("lechuza");
		l1.add("pollo");
//		System.out.println(l1.get(0));
//		System.out.println(l1.get(1));
//		System.out.println("\n");
//
//		
//		//Restablecer
//		l1.set(0, "perro");
//		l1.set(1, "paloma");
//		System.out.println(l1.get(0));
//		System.out.println(l1.get(1));
//		System.out.println("\n");
//
//		
//		//Metodo forEach de ArrayList
//	    l1.forEach(n-> System.out.println(n)); 
//		System.out.println("\n");
//
//	    
//	    //Metodo for (inteligente)
//	    for(String animal : l1) {
//	    	System.out.println(animal);
//	    }
//		System.out.println("\n");
//		
//		//Iterator
//		Iterator i = l1.iterator();
//		while (i.hasNext()) {
//	        System.out.println(i.next());
//		}
		
		
		//Copiar arrayList		
//		String s[] = new String[l1.size()];
//		s = l1.toArray(s);
//		for(String x : s) {
//			System.out.println(x);
		
		
		Car c1 = new Car("3Series",20000, "BMW");
		Car c2 = new Car("Clio",20000, "Renault");
		Car c3 = new Car("Ibiza",20000, "Seat");
		c1.Drive();
		c2.Turn();
		c3.StartEngine();
		
		ArrayList<Car> c = new ArrayList<Car>();
		c.add(c1);
		c.add(c2);
		c.add(c3);
		
		//Imprimiendo con el toString de la clase Arrays
		//System.out.println(Arrays.toString(c.toArray()));
		
		//Imprimiendo
		for(Car x : c) {
			System.out.println(x);
		}
		
			
	}	
}
