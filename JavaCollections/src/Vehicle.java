
public interface Vehicle {
	void Drive();
	void Stop();
	void StartEngine();
	void Turn();
	void GotoITV();
}
