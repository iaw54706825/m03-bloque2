package com.daw.eva.ecom;

import java.util.Scanner;

/**
 *
 * @author eva
 */
public class GestionClientes {

	public static void main(String[] args) {

		// TODO Controlar excepcions aquí al main ...

		ClientJDBCDAO.loadDriver();
		ClientJDBCDAO.connect();
		ProductosJDBCDAO.loadDriver();
		ProductosJDBCDAO.connect();
		boolean salir = false;
		do {
			salir = menuPrincipal();
		} while (!salir);

		ClientJDBCDAO.close();
		ProductosJDBCDAO.close();
	}

	public static boolean menuPrincipal() {
		System.out.println("");
		System.out.println("MENU PRINCIPAL");
		System.out.println("--- Clientes ---");
		System.out.println("1. Listar clientes");
		System.out.println("2. Nuevo cliente");
		System.out.println("3. Modificar cliente");
		System.out.println("4. Eliminar cliente");
		System.out.println("--- Productos ---");
		System.out.println("5. Mostrar productos");
		System.out.println("6. Insertar producto");
		System.out.println("7. Modificar producto");
		System.out.println("8. Eliminar producto");
		System.out.println("--- Join: Clientes - Pedidos ---");
		System.out.println("9. Pedido por cada cliente");
		System.out.println("10. Salir");

		// TODO ampliem menu amb Productes i comandes.....
		// Veure comandes ha de mostrar idProducte, idComanda i noms, quantitat

		Scanner in = new Scanner(System.in);

		int opcion = pideInt("Elige una opción: ");

		switch (opcion) {
		case 1:
			opcionMostrarClientes();
			return false;
		case 2:
			opcionNuevoCliente();
			return false;
		case 3:
			opcionModificarCliente();
			return false;
		case 4:
			opcionEliminarCliente();
			return false;
		case 5:
			opcionMostrarProductos();
			return false;
		case 6:
			opcionNuevoProducto();
			return false;
		case 7:
			opcionModificarProducto();
			return false;
		case 8:
			opcionEliminarProducto();
			return false;
		case 9:
			opcionJoinClientePedidos();
			return false;
		case 10:
			return true;
		default:
			System.out.println("Opción elegida incorrecta");
			return false;
		}

	}

	public static int pideInt(String mensaje) {

		while (true) {
			try {
				System.out.print(mensaje);
				Scanner in = new Scanner(System.in);
				int valor = in.nextInt();
				// in.nextLine();
				return valor;
			} catch (Exception e) {
				System.out.println("No has introducido un número entero. Vuelve a intentarlo.");
			}
		}
	}

	public static String pideLinea(String mensaje) {

		while (true) {
			try {
				System.out.print(mensaje);
				Scanner in = new Scanner(System.in);
				String linea = in.nextLine();
				return linea;
			} catch (Exception e) {
				System.out.println("No has introducido una cadena de texto. Vuelve a intentarlo.");
			}
		}
	}

	public static void opcionMostrarClientes() {
		System.out.println("Listado de Clientes:");
		ClientJDBCDAO.getTablaClientes();
		ClientJDBCDAO.printTablaClientes();
	}

	public static void opcionNuevoCliente() {
		Scanner in = new Scanner(System.in);

		System.out.println("Introduce los datos del nuevo cliente:");
		String nombre = pideLinea("Nombre: ");
		String direccion = pideLinea("Dirección: ");

		boolean res = ClientJDBCDAO.insertCliente(nombre, direccion);

		if (res) {
			System.out.println("Cliente registrado correctamente");
		} else {
			System.out.println("Error :(");
		}
	}

	public static void opcionModificarCliente() {
		Scanner in = new Scanner(System.in);
		int id = pideInt("Indica el id del cliente a modificar: ");
		// Comprobamos si existe el cliente
		if (!ClientJDBCDAO.existsCliente(id)) {
			System.out.println("El cliente " + id + " no existe.");
			return;
		}

		// Mostramos datos del cliente a modificar
		ClientJDBCDAO.printCliente(id);

		// Solicitamos los nuevos datos
		String nombre = pideLinea("Nuevo nombre: ");
		String direccion = pideLinea("Nueva dirección: ");

		// Registramos los cambios
		boolean res = ClientJDBCDAO.updateCliente(id, nombre, direccion);

		if (res) {
			System.out.println("Cliente modificado correctamente");
		} else {
			System.out.println("Error :(");
		}
	}

	public static void opcionEliminarCliente() {
		Scanner in = new Scanner(System.in);

		int id = pideInt("Indica el id del cliente a eliminar: ");

		// Comprobamos si existe el cliente
		if (!ClientJDBCDAO.existsCliente(id)) {
			System.out.println("El cliente " + id + " no existe.");
			return;
		}

		// Eliminamos el cliente
		boolean res = ClientJDBCDAO.deleteCliente(id);

		if (res) {
			System.out.println("Cliente eliminado correctamente");
		} else {
			System.out.println("Error :(");
		}
	}

	
	// ------ PRODUCTOS ---------
	public static void opcionMostrarProductos() {
		ProductosJDBCDAO.mostrarProductos();
	}

	private static void opcionNuevoProducto() {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduce los datos del nuevo cliente:");
		String nombre = pideLinea("Nombre: ");
		String precio = pideLinea("Precio: ");
		boolean res = ProductosJDBCDAO.insertarProducto(nombre, Double.parseDouble(precio));
		if (res) {
			System.out.println("Cliente registrado correctamente");
		} else {
			System.out.println("Error :(");
		}
	}

	private static void opcionModificarProducto() {
		Scanner in = new Scanner(System.in);
		int id = pideInt("Indica el id del producto a modificar: ");
		// Comprobamos si existe el cliente
		if (!ProductosJDBCDAO.existsProducto(id)) {
			System.out.println("El producto " + id + " no existe.");
			return;
		}

		// Solicitamos los nuevos datos
		String nombre = pideLinea("Nuevo nombre: ");
		String precio = pideLinea("Nueva precio: ");

		// Registramos los cambios
		boolean res = ProductosJDBCDAO.modificarProducto(id, nombre, Double.parseDouble(precio));


		if (res) {
			System.out.println("Producto modificado correctamente");
		} else {
			System.out.println("Error :(");
		}
	}

	
		
	private static void opcionEliminarProducto() {
		Scanner in = new Scanner(System.in);

		int id = pideInt("Indica el id del producto a eliminar: ");

		// Comprobamos si existe el cliente
		if (!ClientJDBCDAO.existsCliente(id)) {
			System.out.println("El producto " + id + " no existe.");
			return;
		}

		// Eliminamos el cliente
		boolean res = ProductosJDBCDAO.eliminarProducto(id);

		if (res) {
			System.out.println("Producto eliminado correctamente");
		} else {
			System.out.println("Error :(");
		}
		
	}
	
	// JOIN 
	private static void opcionJoinClientePedidos() {
		Scanner in = new Scanner(System.in);

		int id = pideInt("Indica el id del cliente: ");

		// Comprobamos si existe el cliente
		if (!ClientJDBCDAO.existsCliente(id)) {
			System.out.println("El cliente " + id + " no existe.");
			return;
		}
		ProductosJDBCDAO.pedidosCliente(id);
		
	}

}
