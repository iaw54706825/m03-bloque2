package com.daw.eva.ecom.business.entities;

public class Client {
	private String nombre;
	private String direccion;
	
	
	/**
	 * @param nombre
	 * @param direccion
	 */
	public Client(String nombre, String direccion) {
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	/**
	 * 
	 */
	public Client() {
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}


	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	

	@Override
	public String toString() {
		return "Client [nombre=" + nombre + ", direccion=" + direccion + "]";
	}	

}
