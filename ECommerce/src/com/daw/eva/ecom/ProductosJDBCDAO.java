package com.daw.eva.ecom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProductosJDBCDAO {
	/** Conexion */
	private static Connection conn = null;

	// Configuraci贸n de la conexi贸n a la base de datos
	private static final String DB_HOST = "localhost";
	private static final String DB_PORT = "3306";
	private static final String DB_NAME = "tienda";
	private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME;
	private static final String DB_USER = "root";
	private static final String DB_PASS = "";
	private static final String DB_MSQ_CONN_OK = "CONEXI脫N CORRECTA";
	private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXI脫N";

	// Configuraci髇 de la tabla Productos
	private static final String DB_CLI = "producto";
	private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
	private static final String DB_CLI_ID = "id";
	private static final String DB_CLI_NOM = "nombre";
	private static final String DB_CLI_PREC = "precio";

	//////////////////////////////////////////////////
	// M脡TODOS DE CONEXI脫N A LA BASE DE DATOS
	//////////////////////////////////////////////////

	/**
	 * Intenta cargar el JDBC driver.
	 * 
	 * @return true si pudo cargar el driver, false en caso contrario
	 */
	public static boolean loadDriver() {
		try {
			System.out.print("Loading Driver...");
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			System.out.println("OK!");
			return true;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Intenta conectar con la base de datos.
	 *
	 * @return true si pudo conectarse, false en caso contrario
	 */
	public static boolean connect() {
		try {
			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
			System.out.println("BBDD conectada");
			return true;
		} catch (Exception e) {
			System.out.println("Error, no se ha podido conectar a la BBDD");
			return false;
		}
	}

	/**
	 * Comprueba la conexi贸n y muestra su estado por pantalla
	 *
	 * @return true si la conexi贸n existe y es v谩lida, false en caso contrario
	 */
	public static boolean isConnected() {
		// Comprobamos estado de la conexion
		try {
			return !conn.isClosed();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Cierra la conexi贸n con la base de datos
	 */
	public static void close() {
		try {
			conn.close();
			System.out.println("Conexion cerrada");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Metodos tabla Productos

	public static void mostrarProductos() {
		try {
			Statement stm = conn.createStatement();
			ResultSet ress = stm.executeQuery(DB_CLI_SELECT);
			while (ress.next()) {
				System.out.println(ress.getString(1) + " " + ress.getString(2) + " " + ress.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean insertarProducto(String nombre, double precio) {
		Statement stm;
		try {
			stm = conn.createStatement();
			System.out.println("insert into producto(nombre, precio) values ('" + nombre + "','" + precio + "')");
			stm.executeUpdate("insert into producto(nombre, precio) values ('" + nombre + "','" + precio + "')");
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo insertar");
			e.printStackTrace();
		}
		return false;
	}

	public static boolean modificarProducto(int id, String nuevoNombre, double nuevoPrecio) {
		Statement stm;
		try {
			stm = conn.createStatement();
			stm.executeUpdate(
					"update producto set nombre='" + nuevoNombre + "', precio='" + nuevoPrecio + "' where id=" + id);
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo actualizar");
			e.printStackTrace();
		}
		return false;
	}

	public static boolean eliminarProducto(int id) {
		Statement stm;
		try {
			stm = conn.createStatement();
			stm.executeUpdate("delete from producto where id=" + id);
			return true;
		} catch (SQLException e) {
			System.out.println("No se pudo Eliminar");
			e.printStackTrace();
		}
		return false;
	}

	public static boolean existsProducto(int id) {
		try {
			Statement stm = conn.createStatement();
			ResultSet ress = stm.executeQuery(DB_CLI_SELECT + " WHERE id=" + id);
			if (ress.next()) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static void pedidosCliente(int id) {
		try {
			Statement stm = conn.createStatement();
			//SELECT * from clientes JOIN pedidos on clientes.id = pedidos.cliente WHERE clientes.id = 1
			String consulta = "SELECT * from clientes JOIN pedidos on clientes.id = pedidos.cliente WHERE clientes.id = " + id;
			System.out.println(consulta);
			ResultSet ress = stm.executeQuery(consulta);
			while (ress.next()) {
				System.out.println(ress.getString(1) + " " + ress.getString(2) + " " + ress.getString(3));
				System.out.println("Id pedido: " + ress.getString("numero") + " - Cantidad: " +ress.getString("cantidad"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
