package conectaBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ModificaBD {
	public static void main(String[] args) {
		try {
			//1. Crear conexion
			String ruta = "jdbc:mysql://localhost:3306/tienda";
			String usuario = "root";
			String pass = "";
			Connection conexion = DriverManager.getConnection(ruta, usuario, pass);
			
			//2. Crear objeto statement
			Statement statement = conexion.createStatement();
			
			//Insertar registros
//			String insert= "insert into clientes(nombre, direccion) values ('carlos','barcelona')";
//			statement.executeUpdate(insert);
			
			//Actualizar
			String actualizar = "update clientes set direccion='Andorra' where id=10";
			statement.executeUpdate(actualizar);
			
			//Eliminar
			String eliminar = "delete from clientes where id=10";
			statement.executeUpdate(eliminar);


		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
