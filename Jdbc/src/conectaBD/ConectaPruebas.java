package conectaBD;

import java.sql.*;
public class ConectaPruebas {

	public static void main(String[] args) {
		try {
			//1. Crear conexion
			String ruta = "jdbc:mysql://localhost:3306/tienda";
			String usuario = "root";
			String pass = "";
			Connection conexion = DriverManager.getConnection(ruta, usuario, pass);
			
			//2. Crear objeto statement
			Statement statement = conexion.createStatement();
			
			//3. Ejecutar SQL			
			ResultSet result = statement.executeQuery("select * from clientes");
			
			//4. Recorrer resultado de la consulta
			while (result.next()) {
				System.out.println(result.getString(2) +" " +result.getString(3));
			}
		} catch (Exception e) {
			System.out.println("Error");
		}

	}

}
