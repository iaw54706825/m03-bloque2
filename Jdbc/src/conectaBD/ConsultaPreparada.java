package conectaBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConsultaPreparada {

	public static void main(String[] args) {
		// 

	
		try {
			//1. Crear conexion
			String ruta = "jdbc:mysql://localhost:3306/tienda";
			String usuario = "root";
			String pass = "";
			Connection conexion = DriverManager.getConnection(ruta, usuario, pass);
			
			//2. Preparar consulta
			PreparedStatement sentencia = conexion.prepareStatement("select * from clientes where id=?");
			
			//3. Preparar Consulta
			sentencia.setInt(1, 1);
			
			//4. Resultado consulta
			// 1ra consulta
			ResultSet rs = sentencia.executeQuery();
			while(rs.next()) {
				System.out.println(rs.getString(2));
			}
			rs.close();

			// 2da consulta
			sentencia.setInt(1, 2);
			rs = sentencia.executeQuery();
			while(rs.next()) {
				System.out.println(rs.getString(2));
			}
			rs.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
