package empleado;

public class RRHH extends Empleado {

	public RRHH() {
		super();
	}
	
	public double getSalary(int meses, double salarioMes) {
		return meses * salarioMes;
	}	
}
