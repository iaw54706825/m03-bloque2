/**
 * 
 */
package empleado;

/**
 * @author Carlos
 *
 */
public class Empleado {
	private String id;
	private String nombre;
	private int edad;

	public void setData(String id, String nombre, int edad) {
		this.id = id;
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getData() {
		return "Id: " + this.id + " Nombre: " + this.nombre + " Edad: " + this.edad;
	}
}
