package empleado;

public class Diseny extends Empleado{

	public Diseny() {
		super();
	}
	
	public double getSalary(int semanas, double salarioSemana) {
		return semanas * salarioSemana;
	}	
}
