/**
 * 
 */
package empleado;

/**
 * @author Carlos
 *
 */
public class Tecnologia extends Empleado {

	public Tecnologia() {
		super();
	}
	
	public double getSalary(int horas, double salarioHora) {
		return horas * salarioHora;
	}	

}
