package empleado;

public class Main {

	public static void main(String[] args) {
		//Creamos los empleados
		Diseny d = new Diseny();
		Tecnologia t = new Tecnologia();
		RRHH r = new RRHH();
		
		//Asignamos atributos
		d.setData("a123", "Carlos", 20);
		t.setData("b123", "Juan", 30);
		r.setData("c321", "Cindy", 33);	
		
		//Imprimimos datos
		System.out.println(d.getData());
		System.out.println(t.getData());
		System.out.println(r.getData());
		
		//Obtenemos salarios
		System.out.println(d.getSalary(1, 400));
		System.out.println(t.getSalary(8, 10));
		System.out.println(r.getSalary(1200, 3));
	}
}
