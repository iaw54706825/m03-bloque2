/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Circulo {
	private final double PI = Math.PI;	


	
	
	/**
	 * @param rADIO el rADIO del circulo
	 * return area
	 */
	public double area(double radio) {
		return  PI * radio * radio;
	}
	
	/**
	 * @param rADIO el rADIO del circulo
	 * return parametro
	 */
	public double perimetro(double radio) {
		return  2 * PI * radio;
	}




	public static void main(String[] args) {
		Circulo c = new Circulo();
		System.out.println(c.area(5));
		System.out.println(c.perimetro(5));
		
	}

}
