/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Programador {
	private String nombre = "Juan";
	private int salario = 1000;

	/**
	 * @return el nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return el salario
	 */
	public int getSalario() {
		return salario;
	}

	/**
	 * @param salario el salario a establecer
	 */
	public void setSalario(int salario) {
		this.salario = salario;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Programador p = new Programador();
		System.out.println(p.getNombre());
		System.out.println(p.getSalario());
		p.setNombre("Carlos");
		System.out.println(p.getNombre());


	}
}
